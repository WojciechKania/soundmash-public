
import com.adalbert.soundmash.models.Channel
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.assertThrows
import java.lang.IllegalArgumentException

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ChannelTest {

    @Test
    fun testInsertSamples_GivenStartIndexLargerThanSize_WhenAddingSamples_ThenSizeFits() {
        val channel = Channel()
        channel.insertSamples(FloatArray(10) {1f}, 10)
        assertEquals(20, channel.samplesSize)
        val expectedArray = FloatArray(20) { i -> if (i < 10) 0f else 1f}
        assertArrayEquals(expectedArray, channel.getOriginalSamples())

    }

    @Test
    fun testInsertSamples_GivenStartIndexInsideArrayBounds_WhenAddingSamples_ThenSizeFits() {
        val channel = Channel()
        channel.insertSamples(FloatArray(10) { 1f })
        channel.insertSamples(FloatArray(10) { 2f }, 5)
        assertEquals(20, channel.samplesSize)
        val expectedArray = FloatArray(20) { i -> if (i in 5..14) 2f else 1f}
        assertArrayEquals(expectedArray, channel.getOriginalSamples())
    }

    @Test
    fun testInsertSamples_GivenStartIndexNegative_WhenAddingSamples_ThenExceptionThrown() {
        val channel = Channel()
        assertThrows<IllegalArgumentException> { channel.insertSamples(FloatArray(10) { 1f }, -5) }
    }

    @Test
    fun testInsertSamples_GivenNotifiedAboutSizeAndEmpty_WhenAddingSamples_ThenSizeFitsAndValuesCorrect() {
        val channel = Channel()
        channel.assureSufficientCapacity(20)
        val prevSize = channel.samplesSize
        channel.assureSufficientCapacity(20)
        assertEquals(prevSize, channel.samplesSize)
        channel.insertSamples(FloatArray(10) {1f}, startIndex = 10)
        assertEquals(20, channel.samplesSize)
        val expectedArray = FloatArray(20) { i -> if (i < 10) 0f else 1f}
        assertArrayEquals(expectedArray, channel.getOriginalSamples())
    }

    @Test
    fun testInsertSamples_GivenNotifiedAboutSizeAndNotEmpty_WhenAddingSamples_ThenSizeFitsAndValuesCorrect() {
        val channel = Channel()
        channel.assureSufficientCapacity(30)
        channel.insertSamples(FloatArray(10) {1f})
        channel.insertSamples(FloatArray(10) {2f}, startIndex = 20)
        assertEquals(30, channel.samplesSize)
        val expectedArray = FloatArray(30) { i -> if (i < 10) 1f else if (i < 20) 0f else 2f }
        assertArrayEquals(expectedArray, channel.getOriginalSamples())
    }

    @Test
    fun testRemoveSamples_GivenCorrectRemovingIndices_WhenRemovingSamples_ThenCorrectValuesAreRemovedOthersLeft() {
        val channel = Channel()
        channel.insertSamples(FloatArray(20) {index -> index.toFloat()})
        assertEquals(20, channel.samplesSize)
        channel.removeSamples(5, 10)
        assertEquals(15, channel.samplesSize)
        val expectedArray = listOf(0f, 1f, 2f, 3f, 4f, 10f, 11f, 12f, 13f, 14f, 15f, 16f, 17f, 18f, 19f).toFloatArray()
        assertArrayEquals(expectedArray, channel.getOriginalSamples().sliceArray(0 until channel.samplesSize))
    }

    @Test
    fun testMoveSamples_GivenCorrectMovedIndicesAndDestination_WhenMovingSamples_ThenOutputIsCorrect() {
        val channel = Channel()
        channel.insertSamples(FloatArray(20) {index -> index.toFloat()})
        assertEquals(20, channel.samplesSize)
        channel.moveSamples(5, 10, 0)
        assertEquals(20, channel.samplesSize)
        val expectedArray = listOf(5f, 6f, 7f, 8f, 9f, 0f, 1f, 2f, 3f, 4f, 10f, 11f, 12f, 13f, 14f, 15f, 16f, 17f, 18f, 19f).toFloatArray()
        assertArrayEquals(expectedArray, channel.getOriginalSamples().sliceArray(0 until channel.samplesSize))
    }

    @Test
    fun testMoveSamples_GivenCorrectMovingIndicesAndDestinationOutOfRange_WhenMovingSamples_ThenOutputStretchesOut() {
        val channel = Channel()
        channel.insertSamples(FloatArray(20) {index -> index.toFloat()})
        assertEquals(20, channel.samplesSize)
        channel.moveSamples(5, 10, 50)
        assertEquals(55, channel.samplesSize)
        val check = FloatArray(55) {index -> if (index < 5) index.toFloat() else if (index < 15) index + 5f else if (index < 50) 0f else index - 45f}
        assertArrayEquals(check, channel.getOriginalSamples().sliceArray(0 until channel.samplesSize))
    }

    @Test
    fun testCopySamples_GivenNoValuesInChannel_WhenCopyingSamples_ThenEmptyArrayIsReturned() {
        val channel = Channel()
        val samples = channel.getCopyOfSamples()
        assertEquals(0, samples.size)
    }

    @Test
    fun testCopySamples_GivenValuesInChannel_WhenCopyingSamples_ThenCorrectValuesAreReturned() {
        val channel = Channel()
        channel.insertSamples(FloatArray(10) { index -> index.toFloat()})
        val samples = channel.getCopyOfSamples()
        assertArrayEquals(FloatArray(10) { index -> index.toFloat() }, samples)
    }

    @Test
    fun testCopySamples_GivenCopiedSamples_WhenChangesMadeToThem_ThenValuesInChannelNotChanged() {
        val channel = Channel()
        channel.insertSamples(FloatArray(10) { index -> index.toFloat()})
        val samples = channel.getCopyOfSamples()
        for (index in samples.indices) {
            samples[index] *= 0.5f
            assertEquals(2 * samples[index], channel.getCopyOfSamples(index, index + 1)[0])
        }
    }

    @Test
    fun testCopySamples_GivenGotOriginalSamples_WhenChangesMadeToThem_ThenValuesInChannelChanged() {
        val channel = Channel()
        channel.insertSamples(FloatArray(10) { index -> index.toFloat()})
        val samples = channel.getOriginalSamples()
        for (index in samples.indices)
            samples[index] *= 0.5f
        assertArrayEquals(samples, channel.getCopyOfSamples())
    }

    @Test
    fun testCopySamples_GivenBuffer_WhenSamplesCopied_ThenOnlySelectedValuesChanged() {
        val channel = Channel()
        channel.insertSamples(FloatArray(20) { index -> index.toFloat()})
        val buffer = FloatArray(10) {0f}
        channel.getCopyOfSamples(buffer, 5, 10)
        val check = FloatArray(10) {index -> if (index >= 5) 0f else index + 5f}
        assertArrayEquals(buffer, check)
    }

    @Test
    fun testReplaceSamples_GivenIndicesInRightRange_WhenReplacingValues_ThenValuesInChannelChangedCorrectly() {
        val channel = Channel()
        channel.insertSamples(FloatArray(10) { index -> index.toFloat()})
        val samples = FloatArray(3) {index -> 10f * index}
        channel.replaceSamples(samples)
        val check = FloatArray(10) {index -> if (index < 3) index * 10f else index.toFloat()}
        assertArrayEquals(check, channel.getCopyOfSamples())
    }

    @Test
    fun testClearChannel_GivenChannelWithSamples_WhenChannelCleared_ThenSizeZero() {
        val channel = Channel()
        channel.insertSamples(FloatArray(10) { index -> index.toFloat()})
        channel.clearChannel()
        assertArrayEquals(FloatArray(0), channel.getCopyOfSamples())
    }

    @Test
    fun testPackSamples_GivenChannelWithSamples_WhenDifferentSamplesArePacked_ThenChannelHasOnlyPackedSamples() {
        val channel = Channel()
        channel.insertSamples(FloatArray(10) { index -> index.toFloat()})
        channel.packSamples(FloatArray(5) {1f}, 3)
        assertArrayEquals(FloatArray(3) {1f}, channel.getCopyOfSamples())
    }

}