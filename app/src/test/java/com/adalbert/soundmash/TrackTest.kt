
import com.adalbert.soundmash.models.Track
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertArrayEquals

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TrackTest {

    @Test
    fun testInsertSamples_GivenStereoTrack_WhenValuesArrayWithoutOffsetAndAmountOfSamplesLoaded_ThenAddedCorrectAmountAndValues() {
        val track = Track()
        val buffer = Array (2) { FloatArray(10) {i -> i.toFloat()} }
        track.insertSamples(buffer, 8000, 16)
        assertArrayEquals(buffer[0], track.getOriginalSamples()[0])
        assertArrayEquals(buffer[1], track.getOriginalSamples()[1])
    }

    @Test
    fun testInsertSamples_GivenStereoTrack_WhenValuesArrayWithOffsetWithoutAmountOfSamplesLoaded_ThenAddedCorrectAmountAndValues() {
        val track = Track()
        val buffer = Array (2) { FloatArray(10) {i -> (i + 1).toFloat()} }
        track.insertSamples(buffer, 8000, 16, index = 10)
        assertArrayEquals(buffer[0], track.getOriginalSamples()[0].sliceArray(10 until 20))
        assertArrayEquals(buffer[1], track.getOriginalSamples()[1].sliceArray(10 until 20))
    }


    @Test
    fun testInsertSamples_GivenStereoTrack_WhenValuesArrayWithOffsetWithAmountOfSamplesLoaded_ThenAddedCorrectAmountAndValues() {
        val track = Track()
        val buffer = Array (2) { FloatArray(10) {i -> (i + 1).toFloat()} }
        track.insertSamples(buffer, 8000, 16, index = 10, samplesToLoad = 5)
        val copyOfSamples = track.getOriginalSamples()
        assertArrayEquals(buffer[0].sliceArray(0 until 5), copyOfSamples[0].sliceArray(10 until 15))
        assert(copyOfSamples[0].size == 15)
        assertArrayEquals(buffer[1].sliceArray(0 until 5), copyOfSamples[1].sliceArray(10 until 15))
        assert(copyOfSamples[1].size == 15)
    }

    @Test
    fun testMoveSamples_GivenMonoTrackWithValues_WhenInsertingStereoSignal_ThenChannelsPopulatedWithCorrectValues() {
        val track = Track()
        track.insertSamples(Array (1) { FloatArray(100) {i -> (i + 1).toFloat()} }, 8000, 16)
        assertEquals(1, track.numChannels)
        assertEquals(100, track.numFrames)
        track.insertSamples(Array(2) { FloatArray(100) { i -> i + 1f} }, 8000, 16)
        assertEquals(2, track.numChannels)
        assertEquals(200, track.numFrames)
        val buffer = Array(2) { FloatArray(200) { 0f } }
        val check = FloatArray(200) {i -> (i % 100) + 1f}
        track.getCopyOfSamples(buffer)
        assertArrayEquals(check, buffer[0])
        assertArrayEquals(check, buffer[1])
    }

    @Test
    fun testRemoveSamples_GivenStereoTrack_WhenStartIndexEndIndexNotSpecified_ThenRemovedCorrectly() {
        val track = Track()
        val samples = Array (2) { FloatArray(100) {i -> (i + 1).toFloat()} }
        track.insertSamples(samples, 8000, 16)
        track.removeSamples()
        val buffer = Array(2) {FloatArray(100) {0f}}
        assertEquals(0, track.numFrames)
        track.getCopyOfSamples(buffer)
        assertArrayEquals(buffer[0], FloatArray(100) {0f})
        assertArrayEquals(buffer[1], FloatArray(100) {0f})
    }

    @Test
    fun testClearSamples_GivenStereoTrackWithValues_WhenClearedOut_ThenNoValuesLeftAndStateReset() {
        val track = Track()
        val samples = Array (2) { FloatArray(100) {i -> (i + 1).toFloat()} }
        track.insertSamples(samples, 8000, 16)
        track.clearTrack()
        assertEquals(0, track.numFrames)
        assertEquals(0, track.numChannels)
        assertEquals(0, track.samplingRate)
        assertEquals(0, track.validBits)
        val buffer = Array(2) {FloatArray(100) {0f}}
        track.getCopyOfSamples(buffer)
        assertArrayEquals(buffer[0], FloatArray(100) {0f})
        assertArrayEquals(buffer[1], FloatArray(100) {0f})
    }

    @Test
    fun testGetCopyOfSamplesInSingleArray_GivenStereoTrackWithValues_WhenCopyInSingleArrayRequested_ThenSingleArrayHoldsValuesFromBothChannels() {
        val track = Track()
        val samples = Array (2) { FloatArray(100) {i -> (i + 1).toFloat()} }
        track.insertSamples(samples, 8000, 16)
        val buffer = FloatArray(200) {0f}
        track.getCopyOfSamplesInSingleArray(buffer)
        val check = FloatArray(200) {0f}
        for (index in check.indices step 2) {
            check[index] = index / 2 + 1f
            check[index + 1] = index / 2 + 1f
        }
        assertArrayEquals(buffer, check)
    }

    @Test
    fun testGetCopyOfSamplesInSingleArray_GivenMonoTrackWithValues_WhenCopyInSingleArrayRequested_ThenSingleArrayHoldsAllValues() {
        val track = Track()
        val samples = Array (1) { FloatArray(100) {i -> (i + 1).toFloat()} }
        track.insertSamples(samples, 8000, 16)
        val buffer = FloatArray(100)  {0f}
        track.getCopyOfSamplesInSingleArray(buffer)
        val check = FloatArray(100) {i -> (i + 1).toFloat()}
        assertArrayEquals(buffer, check)
    }

    @Test
    fun testMoveSamples_GivenStereoTrackWithValues_WhenMovingSamples_ThenRequestedValuesAreMoved() {
        val track = Track()
        val samples = Array (2) { FloatArray(100) {i -> (i + 1).toFloat()} }
        track.insertSamples(samples, 8000, 16)
        track.moveSamples(80, 100, 0)
        val buffer = Array(2) {FloatArray(100) {0f}}
        track.getCopyOfSamples(buffer)
        assertArrayEquals(buffer[0], FloatArray(100) {index -> if (index < 20) index + 81f else index - 19f})
        assertArrayEquals(buffer[1], FloatArray(100) {index -> if (index < 20) index + 81f else index - 19f})
    }

    @Test
    fun testMoveSamples_GivenStereoTrackWithValues_WhenMovingSamplesWithNonExistantIndex_ThenTrackStretchedOut() {
        val track = Track()
        val samples = Array (2) { FloatArray(100) {i -> (i + 1).toFloat()} }
        track.insertSamples(samples, 8000, 16)
        track.moveSamples(80, 100, 120)
        assertEquals(140, track.numFrames)
        val buffer = Array(2) {FloatArray(140) {0f}}
        track.getCopyOfSamples(buffer)
        val valueFun = {index: Int ->
            when {
                index < 80 -> index.toFloat() + 1
                index >= 120 -> index - 40f + 1
                else -> 0f
            }
        }
        val check = FloatArray(140) {index -> valueFun(index)}
        assertArrayEquals(check, buffer[0])
        assertArrayEquals(check, buffer[1])
    }

    @Test
    fun testPackSamples_GivenStereoTrackWithValues_WhenPackingSamples_ThenTrackContainsOnlyPacked() {
        val track = Track()
        val samples = Array (2) { FloatArray(100) {i -> (i + 1).toFloat()} }
        track.insertSamples(samples, 8000, 16)
        val newSamples = Array(1) {FloatArray(50) { 3.33f } }
        track.packSamples(newSamples, 16000, 16, newSamples[0].size)
        assertEquals(50, track.numFrames)
        assertArrayEquals(newSamples[0], track.getOriginalSamples()[0])
    }

}