package com.adalbert.soundmash.gestures

import android.view.MotionEvent

/**
 * Gesture context is used in touch gestures delivery
 */
class GestureContext {

    /**
     * A list of registered gesture detectors, some description in [CompleteGestureDetector] docs.
     */
    private val gestureDetectors = mutableListOf<CompleteGestureDetector>()

    fun removeGestureDetector(gestureDetector: CompleteGestureDetector) {
        gestureDetectors.remove(gestureDetector)
    }

    fun addGestureDetector(gestureDetector: CompleteGestureDetector) {
        gestureDetectors.add(gestureDetector)
    }

    /**
     * Passes the registered touch events from the last to the first registered gestures detector, until one of them processes the events
     * @param event The touch event for processing
     */
    fun handleTouchEvent(event : MotionEvent?) : Boolean {
        var eventHandled = false
        var detectorIndex = gestureDetectors.size - 1
        while (detectorIndex >= 0 && !eventHandled) {
            eventHandled = gestureDetectors[detectorIndex].handleTouchEvent(event)
            detectorIndex--
        }
        return eventHandled
    }

}