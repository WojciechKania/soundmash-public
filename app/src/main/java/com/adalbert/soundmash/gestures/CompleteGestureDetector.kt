package com.adalbert.soundmash.gestures

import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import androidx.core.view.GestureDetectorCompat

/**
 * Describes general gesture detector, containing single-touch gesture detector ([mGestureDetector]) and multi-touch scaling gesture detector ([mScaleGestureDetector]).
 */
class CompleteGestureDetector(private val mGestureDetector : GestureDetectorCompat, private val mScaleGestureDetector: ScaleGestureDetector) {

    /**
     * On touch event, the information is processed first by the single-touch gesture detector, then by multi-touch scaling gesture detector
     * @param event An event object, containing the necessary information about touch event
     */
    fun handleTouchEvent(event : MotionEvent?) : Boolean {
        if (mGestureDetector.onTouchEvent(event) && !mScaleGestureDetector.isInProgress)
            return true
        if (mScaleGestureDetector.onTouchEvent(event))
            return true
        return false
    }

}