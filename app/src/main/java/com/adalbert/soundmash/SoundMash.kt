package com.adalbert.soundmash

import android.Manifest
import android.app.Application
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.app.ActivityCompat.requestPermissions
import cafe.adriel.androidaudioconverter.AndroidAudioConverter
import cafe.adriel.androidaudioconverter.callback.ILoadCallback
import com.adalbert.soundmash.audioio.AudioIO
import com.adalbert.soundmash.models.Track
import com.adalbert.soundmash.presenters.ProjectPresenter
import com.adalbert.soundmash.presenters.TrackPresenter
import com.adalbert.soundmash.services.GlobalCacheManager
import com.adalbert.soundmash.views.MainActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception

class SoundMash : Application() {

    private var mProjectPresenter : ProjectPresenter? = null

    fun getProjectPresenter(mainActivity: MainActivity) : ProjectPresenter {
        if (mProjectPresenter == null) mProjectPresenter = ProjectPresenter(mainActivity)
        return mProjectPresenter!!
    }

    override fun onCreate() {
        AudioIO.initializeInstance(this)
        // TODO : Handle failure
        externalCacheDir?.let {
            GlobalCacheManager.externalCacheDir = it
            // TODO : This is suboptimal at best
            it.deleteRecursively()
            it.mkdirs()
        }
        super.onCreate()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        Log.d("LOW_MEMORY", "LOW MEMORY")
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        Log.d("TRIM_MEMORY", "TRIM MEMORY")
    }


}