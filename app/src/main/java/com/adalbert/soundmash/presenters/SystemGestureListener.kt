package com.adalbert.soundmash.presenters

import android.view.MotionEvent
import android.view.ScaleGestureDetector
import com.adalbert.soundmash.utils.DoubleRange
import com.adalbert.soundmash.gestures.CompleteGestureListener
import com.adalbert.soundmash.models.ITrack
import com.adalbert.soundmash.transactions.transactionsImplementations.DeleteAudioTransaction
import com.adalbert.soundmash.services.GlobalCacheManager
import com.adalbert.soundmash.transactions.TransactionManager
import com.adalbert.soundmash.transactions.transactionsImplementations.MoveAudioTransaction
import com.adalbert.soundmash.views.ITrackView
import com.adalbert.soundmash.views.renderables.MoveRenderable

import kotlin.math.min

/**
 * Describes main gesture listener, that handles the touch gestures performed on main track view.
 */
class SystemGestureListener(private val transactionManager: TransactionManager, private val mTrackPresenter: TrackPresenter, private val track: () -> ITrack?, private val trackView: () -> ITrackView?) : CompleteGestureListener {

    // TODO : Move that somewhere reasonable
    private val minBarsPerSecond = 50
    private val maxBarsPerSecond = 1000
    private val removeFlingSpeed = -8000

    private var mIsMoving = false
    private var mMoveIndex : Int? = null
    private val moveIndex : Int
       get() = mMoveIndex!!
    private val mMoveRenderable = MoveRenderable { moveIndex }

    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        e?.let {
            if (mTrackPresenter.waveBarsAmount == 0)
                return true
            if (mIsMoving)
                return handleMovingOnSingleTapUp()
            if (mTrackPresenter.beginSelectionIndex == null || mTrackPresenter.endSelectionIndex == null) {
                val accurateTouchIndex = (e.x / (mTrackPresenter.mBarWidth + mTrackPresenter.mBarDistance) + mTrackPresenter.renderingBeginIndex).toInt()
                return handleSelectionChangeOnSingleTapUp(accurateTouchIndex)
            }
        }
        return false
    }

    private fun handleMovingOnSingleTapUp() : Boolean {
        track()?.let { nonNullTrack ->
            var modifiedMoveIndex = mMoveIndex!!
            if (mMoveIndex!! > mTrackPresenter.beginSelectionIndex!!)
                modifiedMoveIndex -= (mTrackPresenter.endSelectionIndex!! - mTrackPresenter.beginSelectionIndex!!)
            modifiedMoveIndex = min(modifiedMoveIndex * nonNullTrack.samplingRate / mTrackPresenter.mBarsPerSecond, nonNullTrack.numFrames)
            val startIndex = min(mTrackPresenter.beginSelectionIndex!! * nonNullTrack.samplingRate / mTrackPresenter.mBarsPerSecond, nonNullTrack.numFrames)
            val endIndex = min(mTrackPresenter.endSelectionIndex!! * nonNullTrack.samplingRate / mTrackPresenter.mBarsPerSecond, nonNullTrack.numFrames)
            val moveAudioTransaction = MoveAudioTransaction(nonNullTrack, startIndex, endIndex - startIndex, modifiedMoveIndex)
            transactionManager.applyTransaction(moveAudioTransaction) { nonNullTrack.updateTrackChangesListeners() }
        }
        onDoubleTap(null)
        return true
    }

    private fun handleSelectionChangeOnSingleTapUp(accurateTouchIndex : Int) : Boolean {
        if (mTrackPresenter.beginSelectionIndex == null) {
            mTrackPresenter.beginSelectionIndex = accurateTouchIndex
            mTrackPresenter.endSelectionIndex?.let { endSelectionIndex ->
                if (mTrackPresenter.beginSelectionIndex!! > endSelectionIndex) {
                    mTrackPresenter.beginSelectionIndex = endSelectionIndex
                    mTrackPresenter.endSelectionIndex = accurateTouchIndex
                }
            }
        } else if (mTrackPresenter.endSelectionIndex == null) {
            mTrackPresenter.endSelectionIndex = min(accurateTouchIndex, mTrackPresenter.waveBarsAmount)
            mTrackPresenter.beginSelectionIndex?.let { beginSelectionIndex ->
                if (beginSelectionIndex > mTrackPresenter.endSelectionIndex!!) {
                    mTrackPresenter.endSelectionIndex = mTrackPresenter.beginSelectionIndex!!
                    mTrackPresenter.beginSelectionIndex = accurateTouchIndex
                }
            }
        }
        if (mTrackPresenter.beginSelectionIndex == mTrackPresenter.endSelectionIndex) {
            mTrackPresenter.beginSelectionIndex = null
            mTrackPresenter.endSelectionIndex = null
        }
        mTrackPresenter.updateSelectionListeners()
        return true
    }

    override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
        track()?.let { nonNullTrack ->
            if (velocityY < removeFlingSpeed && mTrackPresenter.beginSelectionIndex != null && mTrackPresenter.endSelectionIndex != null) {
                val offset = min(mTrackPresenter.beginSelectionIndex!! * nonNullTrack.samplingRate / mTrackPresenter.mBarsPerSecond, nonNullTrack.numFrames)
                val framesToRemove = min(mTrackPresenter.endSelectionIndex!! * nonNullTrack.samplingRate / mTrackPresenter.mBarsPerSecond, nonNullTrack.numFrames) - offset
                val deleteAudioTransaction = DeleteAudioTransaction(nonNullTrack, GlobalCacheManager.registerFile().absolutePath, offset, framesToRemove)
                transactionManager.applyTransaction(deleteAudioTransaction) { nonNullTrack.updateTrackChangesListeners() }
                mTrackPresenter.beginSelectionIndex = null
                mTrackPresenter.endSelectionIndex = null
                mTrackPresenter.updateSelectionListeners()
                return true
            }
        }
        return false
    }

    override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {
        trackView()?.let {
            if (mIsMoving) {
                val moveIndexRange = DoubleRange(0, mTrackPresenter.waveBarsAmount - 1)
                mMoveIndex = moveIndexRange.cutTo(mMoveIndex!! + distanceX.toInt() / (mTrackPresenter.mBarWidth + mTrackPresenter.mBarDistance)).toInt()
                mTrackPresenter.renderingBeginIndex = mMoveIndex!! - it.mWidth / (2 * (mTrackPresenter.mBarWidth + mTrackPresenter.mBarDistance))
                mTrackPresenter.updateTrackView()
                return true
            }
            mTrackPresenter.renderingBeginIndex = mTrackPresenter.renderingBeginIndex + distanceX.toInt() / (mTrackPresenter.mBarWidth + mTrackPresenter.mBarDistance)
            mTrackPresenter.updateTrackView()
            return true
        }
        return false
    }

    override fun onDoubleTap(e: MotionEvent?): Boolean {
        scaledBeginSelectionIndex = null
        scaledEndSelectionIndex = null
        mTrackPresenter.beginSelectionIndex = null
        mTrackPresenter.endSelectionIndex = null
        mMoveIndex = null
        mIsMoving = false
        trackView()?.unregisterRenderable(mMoveRenderable)
        mTrackPresenter.updateSelectionListeners()
        return true
    }

    override fun onLongPress(e: MotionEvent?) {
        if (!mIsMoving && mTrackPresenter.beginSelectionIndex != null && mTrackPresenter.endSelectionIndex != null) {
            mIsMoving = true
            mMoveIndex = mTrackPresenter.beginSelectionIndex
            trackView()?.registerRenderable(mMoveRenderable)
            mTrackPresenter.renderingBeginIndex = mMoveIndex!! - trackView()!!.mWidth / (2 * (mTrackPresenter.mBarWidth + mTrackPresenter.mBarDistance))
            mTrackPresenter.updateTrackView()
        }
    }

    /**
     * SCALING SEGMENT
     */

    private val barsPerSecondRange = DoubleRange(minBarsPerSecond, maxBarsPerSecond)
    private var scaledBarsPerSecond = mTrackPresenter.mBarsPerSecond.toDouble()
    private var scaledBeginSelectionIndex = mTrackPresenter.beginSelectionIndex?.toDouble()
    private var scaledEndSelectionIndex = mTrackPresenter.endSelectionIndex?.toDouble()

    private lateinit var scalingThread : Thread
    private var scalingThreadSwitch = false

    override fun onScaleBegin(detector: ScaleGestureDetector?): Boolean {
        scalingThreadSwitch = true
        scaledBarsPerSecond = mTrackPresenter.mBarsPerSecond.toDouble()
        scaledBeginSelectionIndex = mTrackPresenter.beginSelectionIndex?.toDouble()
        scaledEndSelectionIndex = mTrackPresenter.endSelectionIndex?.toDouble()
        scalingThread = Thread {
            while (scalingThreadSwitch) {
                Thread.sleep(20)
                mTrackPresenter.mBarsPerSecond = scaledBarsPerSecond.toInt()
                scaledBeginSelectionIndex?.let { mTrackPresenter.beginSelectionIndex = it.toInt() }
                scaledEndSelectionIndex?.let { mTrackPresenter.endSelectionIndex = it.toInt() }
                mTrackPresenter.updateSelectionListeners()
                track()?.updateTrackChangesListeners()
            }
        }
        scalingThread.start()
        return true
    }

    override fun onScale(detector: ScaleGestureDetector?): Boolean {
        detector?.let { scaleDetector ->
            if (barsPerSecondRange.isIn(scaledBarsPerSecond * scaleDetector.scaleFactor)) {
                scaledBeginSelectionIndex = scaledBeginSelectionIndex?.times(scaleDetector.scaleFactor)
                scaledEndSelectionIndex = scaledEndSelectionIndex?.times(scaleDetector.scaleFactor)
            }
            scaledBarsPerSecond = barsPerSecondRange.cutTo(scaledBarsPerSecond * scaleDetector.scaleFactor)
        }
        return true
    }

    override fun onScaleEnd(detector: ScaleGestureDetector?) {
        scalingThreadSwitch = false
        mTrackPresenter.mBarsPerSecond = scaledBarsPerSecond.toInt()
        scaledBeginSelectionIndex?.let { mTrackPresenter.beginSelectionIndex = it.toInt() }
        scaledEndSelectionIndex?.let { mTrackPresenter.endSelectionIndex = it.toInt() }
        Thread {
            mTrackPresenter.updateSelectionListeners()
            track()?.updateTrackChangesListeners()
        }.start()
    }

    /**
     * END SCALING SEGMENT
     */

}