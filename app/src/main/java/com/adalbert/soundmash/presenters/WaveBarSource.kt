package com.adalbert.soundmash.presenters

import com.adalbert.soundmash.presenters.WaveBar

interface WaveBarSource {
    val numChannels : Int
    val numWaveBars : Int
    fun isEmpty() : Boolean
    fun getWaveBar(channelIndex : Int, barIndex: Int): WaveBar
}