package com.adalbert.soundmash.presenters

interface WaveBarDestination {
    fun waveBarsUpdated(waveBarSource : WaveBarSource, barsPerSecond : Int)
}