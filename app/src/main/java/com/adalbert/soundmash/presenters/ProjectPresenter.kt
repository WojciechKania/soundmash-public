package com.adalbert.soundmash.presenters

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.app.ActivityCompat
import cafe.adriel.androidaudioconverter.model.AudioFormat
import com.adalbert.soundmash.R
import com.adalbert.soundmash.audioio.AudioIO
import com.adalbert.soundmash.connections.AudioMiddleman
import com.adalbert.soundmash.connections.Attribute
import com.adalbert.soundmash.effects.AudioEffect
import com.adalbert.soundmash.effects.OnValuesChangedListener
import com.adalbert.soundmash.effects.effectsImplementations.*
import com.adalbert.soundmash.generators.AudioGenerator
import com.adalbert.soundmash.generators.SilenceGenerator
import com.adalbert.soundmash.gestures.CompleteGestureDetector
import com.adalbert.soundmash.models.Track
import com.adalbert.soundmash.services.TrackPlayer
import com.adalbert.soundmash.transactions.TransactionManager
import com.adalbert.soundmash.connections.connectionsImplementations.TrackCopyAudioSource
import com.adalbert.soundmash.transactions.transactionsImplementations.GenerateAudioTransaction
import com.adalbert.soundmash.transactions.transactionsImplementations.ApplyEffectTransaction
import com.adalbert.soundmash.transactions.transactionsImplementations.LoadFileTransaction
import com.adalbert.soundmash.services.GlobalCacheManager
import com.adalbert.soundmash.views.MainActivity
import com.adalbert.soundmash.views.TopFragment
import com.codekidlabs.storagechooser.StorageChooser
import effects.CombFilter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.export_settings_layout.view.*
import kotlinx.android.synthetic.main.fragment_top.*
import kotlin.math.min

class ProjectPresenter(private var mMainActivity: MainActivity) : SelectionListener {

    private val mPermissionRequestCode = 4125
    private lateinit var mStorageChooser : StorageChooser

    private val mTrack = Track()
    private val mTrackPresenter = TrackPresenter( this, 200, 1, 1)

    private val trackPlayer = TrackPlayer()

    private val supportedEffects : Array<AudioEffect> = arrayOf(CombFilter(), ChorusEffect(), ReverbEffect(), DistortionEffect(), AlternatingPanningEffect(), AmplitudeChangeEffect())
    private val supportedGenerators : Array<AudioGenerator> = arrayOf(SilenceGenerator())

    private val transactionManager = TransactionManager(mMainActivity)

    /**
     * This initialization method gears up all the needed dependencies in the beginning of application's lifecycle.
     */
    init {
        initPermissions()
        mTrackPresenter.registerTrack(mTrack)
        mTrack.registerListener(mTrackPresenter)
        mTrackPresenter.setWaveBarDestination( mMainActivity.trackView)
        mTrackPresenter.addSelectionListener( mMainActivity.trackView)
        mTrackPresenter.addSelectionListener(this)
        mTrackPresenter.registerTrackView( mMainActivity.trackView)
        val globalGestureListener = SystemGestureListener(transactionManager, mTrackPresenter, { mTrackPresenter.track }, { mTrackPresenter.trackView })
        val globalGestureDetector = mMainActivity.trackView.registerGestureDetector(globalGestureListener)
        mTrackPresenter.mGestureContext.addGestureDetector(globalGestureDetector)
        mMainActivity.trackView.registerGestureContext(mTrackPresenter.mGestureContext)
        mMainActivity.trackView.setOnClickListener {}
    }

    private fun initPermissions() {
        val permissionRead = mMainActivity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
        val permissionWrite = mMainActivity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val neededPermissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (permissionRead != PackageManager.PERMISSION_GRANTED || permissionWrite!= PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mMainActivity, neededPermissions, mPermissionRequestCode)
        }
    }

    /**
     * Uses external library - StorageChooser - for exploring file system and importing audio.
     */
    @SuppressLint("RestrictedApi")
    private fun initFileImport() {
        mStorageChooser = StorageChooser.Builder()
            .withActivity(mMainActivity)
            .withFragmentManager(mMainActivity.fragmentManager)
            .withMemoryBar(true)
            .allowCustomPath(true)
            .setType(StorageChooser.FILE_PICKER)
            .build()

        mStorageChooser.setOnSelectListener {
            val beginSelectionIndex : Int? = mTrackPresenter.beginSelectionIndex
            val endSelectionIndex : Int? = mTrackPresenter.endSelectionIndex
            if (endSelectionIndex == null) {
                val trackOffset = min(beginSelectionIndex?.times(mTrack.samplingRate / mTrackPresenter.mBarsPerSecond) ?: 0, mTrack.numFrames)
                val fileLoadTransaction = LoadFileTransaction(it, mTrack, offsetDestination = trackOffset, framesToLoad = null)
                transactionManager.applyTransaction(fileLoadTransaction)  {
                    updateViews()
                    updateUndoButton(transactionManager.transactionsAmount > 0)
                }
            }
        }
    }

    /**
     * Uses external library - StorageChooser - for exploring file system and exporting audio.
     */
    private fun initFileExport(fileName : String, fileFormat : String) {
        mStorageChooser = StorageChooser.Builder()
            .withActivity(mMainActivity)
            .withFragmentManager(mMainActivity.fragmentManager)
            .withMemoryBar(true)
            .allowCustomPath(true)
            .setType(StorageChooser.DIRECTORY_CHOOSER)
            .build()

        mStorageChooser.setOnSelectListener {
            val builder = AlertDialog.Builder(mMainActivity)
            var progressDialog : Dialog? = null
            builder.setMessage("Writing audio...")
            mMainActivity.runOnUiThread() {
                progressDialog = builder.create()
                progressDialog?.setCanceledOnTouchOutside(false)
                progressDialog?.show()
            }
            AudioIO.AudioWriterInstance.writeFrames("$it/$fileName", AudioFormat.valueOf(fileFormat),
                TrackCopyAudioSource(mTrack),
                audioMiddleman = object : AudioMiddleman {
                    override fun operationFinished(success: Boolean, message: String?, throwable: Throwable?) {
                        Log.e("AUDIO_WRITE", message, throwable)
                        progressDialog?.dismiss()
                        if (!success) {
                            builder.setTitle("Audio writing error")
                                .setMessage(message)
                                .setIcon(R.drawable.error)
                        } else {
                            builder.setTitle("Audio written")
                                .setMessage("Audio has been successfully written")
                        }
                        mMainActivity.runOnUiThread() { builder.create().show() }
                    }
                }
            )
        }
    }

    /**
     * Clears project metadata and data, so that new files with different audio parameters can be imported.
     */
    @SuppressLint("RestrictedApi")
    fun clearProject() {
        mMainActivity.runOnUiThread { mMainActivity.fbUndo.visibility = View.GONE }
        transactionManager.clearTransactions() { updateUndoButton(transactionManager.transactionsAmount > 0) }
        mTrackPresenter.clearTrackPresenter()
        mTrack.clearTrack()
        updateViews()
    }

    /**
     * Uses main thread delivered by the main activity to perform view's update
     */
    private fun updateViews() {
        mMainActivity.runOnUiThread {
            mTrack.updateTrackChangesListeners()
            mTrackPresenter.updateSelectionListeners()
        }
    }

    /**
     * Handling UI events passed from the main activity
     */
    @SuppressLint("RestrictedApi")
    @ExperimentalStdlibApi
    fun handleClick(viewID : Int) {
        when (viewID) {
            R.id.fbImport -> {
                if (transactionManager.transactionsAmount == 0 && mTrack.numFrames == 0) {
                    importSoundFromChosenFile()
                } else {
                    val builder = AlertDialog.Builder(mMainActivity)
                    builder.setTitle("Confirmation")
                        .setMessage("This action will discard the applied changes, continue?")
                        .setNegativeButton("No") { _, _ -> }
                        .setPositiveButton("Yes") { _, _ -> clearProject(); importSoundFromChosenFile() }
                    val dialog = builder.create()
                    dialog.show()
                }
            }
            R.id.fbExport -> {
                val builder = AlertDialog.Builder(mMainActivity)
                val dialogView = mMainActivity.layoutInflater.inflate(R.layout.export_settings_layout, null)
                val spinnerAdapter = ArrayAdapter<AudioFormat>(
                    mMainActivity, android.R.layout.simple_spinner_item, AudioFormat.values()
                )
                spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                dialogView.spinAudioFormats.adapter = spinnerAdapter
                builder.setView(dialogView)
                    .setPositiveButton("Export",
                        DialogInterface.OnClickListener { _, _ ->
                            initFileExport(dialogView.etFilename.text.toString(), dialogView.spinAudioFormats.selectedItem.toString())
                            mStorageChooser.show()
                        })
                    .setNegativeButton("Cancel", DialogInterface.OnClickListener { _, _ -> })
                val dialog = builder.create()
                dialog.show()
            }
            R.id.fbPlay -> {
                trackPlayer.playSelection(mTrack,
                    mTrackPresenter.beginSelectionIndex?.times(mTrack.samplingRate)?.div(mTrackPresenter.mBarsPerSecond) ?: 0,
                    mTrackPresenter.endSelectionIndex?.times(mTrack.samplingRate)?.div(mTrackPresenter.mBarsPerSecond) ?: mTrack.numFrames
                )
            }
            R.id.fbStop -> {
                trackPlayer.stopPlaying()
            }
            R.id.fbMenu -> {
                val builder = AlertDialog.Builder(mMainActivity)
                val supportedEffectsNames = Array(supportedEffects.size) {index -> supportedEffects[index].effectName}
                builder.setTitle("Effects")
                    .setItems(supportedEffectsNames,
                        DialogInterface.OnClickListener { _, which ->
                            gatherAttributesAndStartEffect(supportedEffects[which])
                        })
                builder.create().show()
            }
            R.id.fbInsert -> {
                val builder = AlertDialog.Builder(mMainActivity)
                // TODO : That, sir, is a hack - brutal and inelegant insertion instead of manageable abstraction
                val supportedGeneratorsNames = Array(supportedGenerators.size + 1) {index -> if (index < supportedGenerators.size) supportedGenerators[index].generatorName else ""}
                supportedGeneratorsNames[supportedGenerators.size] = "Insert audio from a file"
                builder.setTitle("Insert audio")
                    .setItems(supportedGeneratorsNames,
                        DialogInterface.OnClickListener { _, which ->
                            if (which < supportedGenerators.size) {
                                val generator = supportedGenerators[which]
                                AttributesDialogService.showAttributesDialog(mMainActivity, generator.attributesDescription, generator.attributesMap) {
                                    val offset = min(mTrackPresenter.beginSelectionIndex!! * mTrack.samplingRate / mTrackPresenter.mBarsPerSecond, mTrack.numFrames)
                                    val audioGenerationTransaction = GenerateAudioTransaction(generator, mTrack, offset)
                                    transactionManager.applyTransaction(audioGenerationTransaction) {
                                        updateViews()
                                        updateUndoButton(transactionManager.transactionsAmount > 0)
                                    }
                                }
                            } else {
                                when (supportedGeneratorsNames[which]) {
                                    "Insert audio from a file" -> {
                                        importSoundFromChosenFile()
                                    }
                                    else -> {}
                                }
                            }
                        })
                builder.create().show()
            }
            R.id.fbUndo -> {
                transactionManager.revertTransaction {
                    if (mTrackPresenter.isEmpty())
                        clearProject()
                    updateViews()
                    updateUndoButton(transactionManager.transactionsAmount > 0)
                }

            }
        }
    }

    fun importSoundFromChosenFile() {
        initFileImport()
        mStorageChooser.show()
    }

    /**
     * Externally visible method for file import, used when transitioning between the audio recorder and main activity
     */
    fun importSoundFromFile(filePath: String) {
        val trackOffset = min(mTrackPresenter.beginSelectionIndex?.times(mTrack.samplingRate / mTrackPresenter.mBarsPerSecond) ?: 0, mTrack.numFrames)
        val fileLoadTransaction = LoadFileTransaction(filePath, mTrack, offsetDestination = trackOffset, framesToLoad = null)
        transactionManager.applyTransaction(fileLoadTransaction)  {
            updateViews()
            updateUndoButton(transactionManager.transactionsAmount > 0)
        }
    }

    fun runOnUIThread(runnable: () -> Unit) {
        mMainActivity.runOnUiThread(runnable)
    }

    fun packSound(audioData : Array<FloatArray>, samplingRate : Int, validBits : Int, samplesToLoad : Int) {
        mTrack.packSamples(audioData, samplingRate, validBits, samplesToLoad)
    }

    private val topFragment = TopFragment()

    /**
     * A helper method, used to display effects parameters and manage UI elements accordingly.
     */
    @SuppressLint("RestrictedApi")
    private fun gatherAttributesAndStartEffect (effect : AudioEffect) {
        effect.effectGestureListener?.let {
            val effectGestureDetector = mMainActivity.trackView.registerGestureDetector(it)
            mTrackPresenter.mGestureContext.addGestureDetector(effectGestureDetector)
            mMainActivity.runOnUiThread {
                mMainActivity.fbMenu.visibility = View.GONE
                mMainActivity.fbApply.visibility = View.VISIBLE
                mMainActivity.fbCancel.visibility = View.VISIBLE
            }
            if (!topFragment.isAdded)
                mMainActivity.supportFragmentManager.beginTransaction().add(R.id.fragment_view_container, topFragment).show(topFragment).commitAllowingStateLoss()
            else
                mMainActivity.supportFragmentManager.beginTransaction().show(topFragment).commitAllowingStateLoss()
            Thread {
                Thread.sleep(200)
                it.addOnValuesChangedListener(object : OnValuesChangedListener {
                    override fun onValuesChanged(valuesChanged: List<Attribute>) {
                        AttributesDialogService.updateTopFragment(mMainActivity, topFragment, valuesChanged)
                    }
                })
                topFragment.btnShowAttributesDialog.setOnClickListener {
                    AttributesDialogService.showAttributesDialog(mMainActivity, effect.attributesDescription, effect.attributesMap) {
                        disableEffectUI(effectGestureDetector, topFragment)
                        startEffect(effect)
                    }
                }
            }.start()
            mMainActivity.fbApply.setOnClickListener {
                disableEffectUI(effectGestureDetector, topFragment)
                startEffect(effect)
            }
            mMainActivity.fbCancel.setOnClickListener {
                disableEffectUI(effectGestureDetector, topFragment)
            }
            return
        }
        AttributesDialogService.showAttributesDialog(mMainActivity, effect.attributesDescription, effect.attributesMap) {
            startEffect(effect)
        }
    }

    @SuppressLint("RestrictedApi")
    private fun startEffect(effect: AudioEffect) {
        val offset = min(mTrackPresenter.beginSelectionIndex!! * mTrack.samplingRate / mTrackPresenter.mBarsPerSecond, mTrack.numFrames)
        val framesToLoad = min(mTrackPresenter.endSelectionIndex!! * mTrack.samplingRate / mTrackPresenter.mBarsPerSecond, mTrack.numFrames) - offset
        val effectApplyTransaction = ApplyEffectTransaction(effect, mTrack, GlobalCacheManager.registerFile().absolutePath, offset, framesToLoad)
        transactionManager.applyTransaction(effectApplyTransaction) {
            updateViews()
            updateUndoButton(transactionManager.transactionsAmount > 0)
        }
    }

    /*********************************************************
     *************  <<  UI UTILITY METHODS  >>  **************
     *********************************************************/

    @SuppressLint("RestrictedApi")
    private fun disableEffectUI(effectGestureDetector: CompleteGestureDetector, topFragment : TopFragment) {
        mMainActivity.supportFragmentManager.beginTransaction().hide(topFragment).commitAllowingStateLoss()
        mTrackPresenter.mGestureContext.removeGestureDetector(effectGestureDetector)
        mMainActivity.runOnUiThread {
            mMainActivity.fbApply.visibility = View.GONE
            mMainActivity.fbCancel.visibility = View.GONE
            mMainActivity.fbMenu.visibility = View.VISIBLE
        }
    }

    @SuppressLint("RestrictedApi")
    private fun updateUndoButton(areThereAnyTransactions : Boolean) {
        mMainActivity.runOnUiThread {
            when (areThereAnyTransactions) {
                true -> mMainActivity.fbUndo.visibility = View.VISIBLE
                false -> mMainActivity.fbUndo.visibility = View.GONE
            }
        }

    }

    @SuppressLint("RestrictedApi")
    override fun selectionUpdated(beginSelection: Int?, endSelection: Int?) {
        mMainActivity.runOnUiThread {
            when {
                beginSelection != null && endSelection != null -> {
                    mMainActivity.fbMenu.visibility = View.VISIBLE
                    mMainActivity.fbInsert.visibility = View.GONE
                }
                beginSelection != null -> {
                    mMainActivity.fbMenu.visibility = View.GONE
                    mMainActivity.fbInsert.visibility = View.VISIBLE
                }
                else -> {
                    mMainActivity.fbMenu.visibility = View.GONE
                    mMainActivity.fbInsert.visibility = View.GONE
                }
            }
        }
    }

}