package com.adalbert.soundmash.presenters

/**
 * Objects of this class keep information about the bars rendered by UI to compose audio track view
 * @property rmsPositive The positive value of RMS
 * @property rmsNegative The negative value of RMS - currently unused
 * @property maxPositive The positive extreme of signal values
 * @property minNegative The negative extreme of signal values
 */
class WaveBar(val rmsPositive : Float, val rmsNegative : Float,
              val maxPositive : Float, val minNegative : Float) {

    override fun toString(): String {
        return "{rms+:$rmsPositive, rms-:$rmsNegative, max+:$maxPositive, min-:$minNegative}"
    }
}