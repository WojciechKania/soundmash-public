package com.adalbert.soundmash.presenters

import android.util.Log
import com.adalbert.soundmash.utils.DoubleRange
import com.adalbert.soundmash.gestures.GestureContext
import com.adalbert.soundmash.models.ITrack
import com.adalbert.soundmash.models.TrackListener
import com.adalbert.soundmash.views.ITrackView
import kotlin.math.max
import kotlin.math.sqrt

/**
 * Used for generating the information visible in the audio track view, and managing selection.
 */
class TrackPresenter(private val mProjectPresenter : ProjectPresenter, var mBarsPerSecond : Int = 200, var mBarWidth : Int = 1, var mBarDistance : Int = 0) : TrackListener, WaveBarSource {

    private var mChannelsWaveBars : Array<Array<WaveBar>> = Array(0) { Array(0) {WaveBar(0f, 0f, 0f, 0f)} }
    val waveBarsAmount : Int
        get() {
            return if (mChannelsWaveBars.isEmpty()) 0
            else mChannelsWaveBars[0].size
        }

    override val numWaveBars: Int
        get() = waveBarsAmount

    private var mTrack : ITrack? = null
    val track : ITrack?
        get() = mTrack

    private var mTrackView : ITrackView? = null
    val trackView : ITrackView?
        get() {
            return mTrackView
        }

    private val mSelectionListeners : MutableList<SelectionListener> = mutableListOf()
    private var mWaveBarDestination : WaveBarDestination? = null

    val mGestureContext : GestureContext = GestureContext()

    override val numChannels: Int
        get() {
            return if (mChannelsWaveBars.isEmpty()) 0
            else mChannelsWaveBars.size
        }


    private var mRenderingBeginIndex : Int = 0
    var renderingBeginIndex : Int
        get() = mRenderingBeginIndex
        set(value) {
            trackView?.let{
                val renderingBeginIndexRange = DoubleRange(0, max(0, waveBarsAmount - it.mWidth / (mBarWidth + mBarDistance)))
                mRenderingBeginIndex = renderingBeginIndexRange.cutTo(value).toInt()
            }
        }
    private var mBeginSelectionIndex : Int? = null
    var beginSelectionIndex : Int?
        get() = mBeginSelectionIndex
        set(value) {
            mBeginSelectionIndex = when (value == null) {
                true -> null
                false -> {
                    val beginSelectionIndexRange = DoubleRange(0, waveBarsAmount)
                    beginSelectionIndexRange.cutTo(value).toInt()
                }
            }
        }
    private var mEndSelectionIndex : Int? = null
    var endSelectionIndex : Int?
        get() = mEndSelectionIndex
        set(value) {
            mEndSelectionIndex = when (value == null) {
                true -> null
                false -> {
                    val endSelectionIndexRange = DoubleRange(0, waveBarsAmount)
                    endSelectionIndexRange.cutTo(value).toInt()
                }
            }
        }

    private val constantWaveBar = WaveBar(0.0f,0.0f,0.0f,0.0f)

    override fun dataUpdated() {
        mTrack?.let { nonNullTrack ->
            val startTime = System.currentTimeMillis()
            val samplesPerBar = nonNullTrack.samplingRate / mBarsPerSecond
            if (nonNullTrack.numFrames == 0) {
                mChannelsWaveBars = Array(nonNullTrack.numChannels) { Array(0) { constantWaveBar } }
                return
            }
            if (mChannelsWaveBars.isEmpty() || mChannelsWaveBars[0].size != (nonNullTrack.numFrames / samplesPerBar))
                mChannelsWaveBars = Array(nonNullTrack.numChannels) { Array(nonNullTrack.numFrames / samplesPerBar) { constantWaveBar } }
            val channelsData = nonNullTrack.getOriginalSamples()
            for (channelIndex in channelsData.indices) {
                // ATTENTION : You can't use channelData.size, use numFrames of the track instead
                val channelData = channelsData[channelIndex]
                val waveBars = mChannelsWaveBars[channelIndex]
                var pixelIndex = 0
                var offset = 0
                var numberOfSamples: Int
                // You are throwing away the last portion of data if it the sampling isn't devisible by mBarsPerSecond
                while (pixelIndex * samplesPerBar < nonNullTrack.numFrames && pixelIndex < waveBars.size) {
                    val nextSamplesBundleIndex = (pixelIndex + 1) * samplesPerBar
                    numberOfSamples = when (nextSamplesBundleIndex < nonNullTrack.numFrames) {
                        true -> (pixelIndex + 1) * samplesPerBar - pixelIndex * samplesPerBar
                        false -> nonNullTrack.numFrames - pixelIndex * samplesPerBar
                    }
                    waveBars[pixelIndex] = calculateRMSsAndExtremes(channelData, offset, numberOfSamples)
                    offset += numberOfSamples
                    pixelIndex += 1
                }
            }
            Log.d("WAVEBARS_CALCULATION", "WaveBars calculated after ${System.currentTimeMillis() - startTime} ms")
            updateWaveBarDestination()
        }
    }

    private fun calculateRMSsAndExtremes(samples : FloatArray, offset : Int, samplesAmount : Int) : WaveBar {
        var sumOfValuesSquared = 0.0f
        val cheatFrames = 5
        var maxValue = -Float.MAX_VALUE
        var minValue = Float.MAX_VALUE
        var amplitudeIndex = offset
        val lastIndex = offset + samplesAmount
        while (amplitudeIndex < lastIndex) {
            sumOfValuesSquared += samples[amplitudeIndex] * samples[amplitudeIndex]
            if (samples[amplitudeIndex] > maxValue)
                maxValue = samples[amplitudeIndex]
            if (samples[amplitudeIndex] < minValue)
                minValue = samples[amplitudeIndex]
            amplitudeIndex += cheatFrames
        }
        val rmsPositive = sqrt( sumOfValuesSquared / (samplesAmount / cheatFrames))
        val rmsNegative = -sqrt(sumOfValuesSquared / (samplesAmount / cheatFrames))
        if (maxValue == -Float.MAX_VALUE) maxValue = 0f
        if (minValue == Float.MAX_VALUE) minValue = 0f
        return WaveBar(rmsPositive, rmsNegative, maxValue, minValue)
    }

    fun clearTrackPresenter() {
        mChannelsWaveBars = Array(0) { Array(0) {WaveBar(0f, 0f, 0f, 0f)} }
        beginSelectionIndex = null
        endSelectionIndex = null
        renderingBeginIndex = 0
    }

    override fun isEmpty(): Boolean {
        return numWaveBars == 0
    }

    override fun getWaveBar(channelIndex : Int, barIndex: Int): WaveBar {
        // These checks are needed here, because of a possible thread desynchronization
        mChannelsWaveBars?.let {
            it[channelIndex]?.let {channelBars ->
                if (barIndex < channelBars.size)
                    return channelBars[barIndex]
            }
        }
        return WaveBar(0f ,0f ,0f ,0f )
    }

    /*********************************************************
     **********  <<  LISTENERS UTILITY METHODS  >>  **********
     *********************************************************/

    fun setWaveBarDestination(listener: WaveBarDestination) {
        mWaveBarDestination = listener
    }

    private fun updateWaveBarDestination() {
        mProjectPresenter.runOnUIThread { mWaveBarDestination?.waveBarsUpdated(this, mBarsPerSecond) }
    }

    fun addSelectionListener(listener: SelectionListener) {
        mSelectionListeners.add(listener)
    }

    fun removeSelectionListener(listener: SelectionListener) {
        mSelectionListeners.remove(listener)
    }

    fun updateSelectionListeners() {
        mProjectPresenter.runOnUIThread { mSelectionListeners.forEach { it.selectionUpdated(mBeginSelectionIndex, mEndSelectionIndex) } }
    }

    fun registerTrack(track : ITrack) {
        this.mTrack = track
    }

    fun registerTrackView(trackView : ITrackView) {
        this.mTrackView = trackView
        mTrackView!!.mBarWidth = mBarWidth
        mTrackView!!.mBarDistance = mBarDistance
        mTrackView!!.mRenderingBeginIndex = 0
    }

    fun updateTrackView() {
        mProjectPresenter.runOnUIThread {
            mTrackView?.let {
                it.mBarWidth = mBarWidth
                it.mBarDistance = mBarDistance
                it.mRenderingBeginIndex = mRenderingBeginIndex
                it.repaint()
            }
        }
    }

}