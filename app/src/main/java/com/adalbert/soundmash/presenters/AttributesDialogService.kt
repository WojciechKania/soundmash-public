package com.adalbert.soundmash.presenters

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.adalbert.soundmash.R
import com.adalbert.soundmash.connections.Attribute
import com.adalbert.soundmash.views.TopFragment
import kotlinx.android.synthetic.main.attribute_input_layout.view.*
import kotlinx.android.synthetic.main.attribute_input_layout.view.tvAttributeMessage
import kotlinx.android.synthetic.main.attribute_input_layout.view.tvAttributeUnit
import kotlinx.android.synthetic.main.attribute_output_layout.view.*
import kotlinx.android.synthetic.main.custom_dialog.view.*
import kotlinx.android.synthetic.main.fragment_top.*
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

object AttributesDialogService {

    private val mAttributesEditFields = mutableListOf<Pair<Attribute, EditText>>()

    fun showAttributesDialog(appContext : Activity, attributes : List<Attribute>, valuesMap : MutableMap<String, Double>, consumeAttributes : () -> Unit) {
        if (attributes.isEmpty()) {
            consumeAttributes()
            return
        }

        val builder = AlertDialog.Builder(appContext)
        val dialogView = appContext.layoutInflater.inflate(R.layout.custom_dialog, null)
        for (attribute in attributes) {
            val attributeView = appContext.layoutInflater.inflate(R.layout.attribute_input_layout, null)
            attributeView.tvAttributeMessage.text = attribute.message
            val setValue = (valuesMap[attribute.attributeID] ?: attribute.defaultValue)
            val symbols = DecimalFormatSymbols(Locale.US)
            symbols.decimalSeparator = '.'
            val format = DecimalFormat("#0.##", symbols)
            attributeView.etAttributeValue.setText(format.format(setValue))
            attributeView.tvAttributeUnit.text = attribute.unit
            attributeView.tvAttributeRange.text = attribute.range.toString()
            mAttributesEditFields.add(Pair(attribute, attributeView.etAttributeValue))
            dialogView.dialog_container.addView(attributeView, LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT))
        }
        builder.setView(dialogView)
            .setPositiveButton("Apply",
                DialogInterface.OnClickListener { dialog, _ ->
                    for (attributeAndEditField in mAttributesEditFields) {
                        try {
                            val parsedValue = attributeAndEditField.second.text.toString().toDouble()
                            if (attributeAndEditField.first.range.isInIncluding(parsedValue)) {
                                valuesMap[attributeAndEditField.first.attributeID] = parsedValue
                            } else {
                                reportMistake(dialog, attributeAndEditField, appContext)
                                return@OnClickListener
                            }
                        } catch (ex : Exception) {
                            return@OnClickListener
                        }
                    }
                    mAttributesEditFields.clear()
                    consumeAttributes()
                })
            .setNegativeButton("Cancel", DialogInterface.OnClickListener { _, _ -> })
        appContext.runOnUiThread { builder.create().show() }
    }

    fun updateTopFragment(appContext : AppCompatActivity, topFragment: TopFragment, valuesChanged : List<Attribute>) {
        topFragment.last_modified_container.removeAllViews()
        for (attribute in valuesChanged) {
            val attributeView = appContext.layoutInflater.inflate(R.layout.attribute_output_layout, null)
            attributeView.tvAttributeMessage.text = attribute.message
            val symbols = DecimalFormatSymbols(Locale.US)
            symbols.decimalSeparator = '.'
            val format = DecimalFormat("#0.##", symbols)
            attributeView.tvAttributeValue.text = format.format(attribute.defaultValue)
            attributeView.tvAttributeUnit.text = attribute.unit
            topFragment.last_modified_container.addView(attributeView, LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT))
        }
    }

    private fun reportMistake(dialog: DialogInterface, attributeAndEditField : Pair<Attribute, EditText>, appContext: Activity) {
        dialog.dismiss()
        mAttributesEditFields.clear()
        val builder = AlertDialog.Builder(appContext)
        builder.setTitle("Incorrect attribute value!")
            .setMessage("Attribute ${attributeAndEditField.first.message} is out of approved range (must be within ${attributeAndEditField.first.range})")
        appContext.runOnUiThread { builder.create().show() }
    }
}