package com.adalbert.soundmash.presenters

/**
 * Describes an entity, that needs to be informed when user selects an audio track fragment.
 */
interface SelectionListener {
    fun selectionUpdated(beginSelection : Int?, endSelection : Int?)
}