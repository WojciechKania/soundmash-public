package com.adalbert.soundmash.effects.effectsTouchListeners
import android.view.MotionEvent
import com.adalbert.soundmash.connections.Attribute
import com.adalbert.soundmash.utils.DoubleRange
import com.adalbert.soundmash.effects.EffectGestureListener
import com.adalbert.soundmash.effects.OnValuesChangedListener
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sign

class AmplitudeChangeGestureListener(attributesMap: MutableMap<String, Double>) : EffectGestureListener(attributesMap) {

    private val valuesChangedListeners = mutableListOf<OnValuesChangedListener>()

    override fun addOnValuesChangedListener(listener: OnValuesChangedListener) {
        valuesChangedListeners.add(listener)
    }

    override fun removeOnValuesChangedListener(listener : OnValuesChangedListener) {
        valuesChangedListeners.remove(listener)
    }

    override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {
        attributesMap["power"] = min(max(((attributesMap["power"] ?: 1.0) + sign(distanceY) / 100f), 0.0), 5.0)
        for (listener in valuesChangedListeners)
            listener.onValuesChanged(listOf(Attribute("power", "Power:", DoubleRange(0.0, 5.0), attributesMap["power"]!!, "")))
        return true
    }
}