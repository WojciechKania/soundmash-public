package com.adalbert.soundmash.effects.effectsImplementations

import com.adalbert.soundmash.connections.Attribute
import com.adalbert.soundmash.effects.AudioEffect
import com.adalbert.soundmash.effects.EffectGestureListener
import com.adalbert.soundmash.connections.AudioInfo
import com.adalbert.soundmash.connections.AudioMiddleman
import com.adalbert.soundmash.utils.DoubleRange
import java.lang.IllegalArgumentException
import kotlin.random.Random

class ReverbEffect : AudioEffect() {

    private val mDefaultDelayStart = 0.25
    private val mDefaultDelayEnd = 0.5
    private val mDefaultPower = 0.1f
    private val mDefaultCombFiltersAmount = 4.0

    private var mReverbBuffer : MutableList<Array<FloatArray>>? = null
    private var mReverbOffsets : MutableList<IntArray>? = null

    override val effectGestureListener: EffectGestureListener? = null

    override val attributesDescription: List<Attribute> = listOf(
        Attribute("delayStart", "Reverb delay range start", DoubleRange(0.0, 10.0), mDefaultDelayStart, "seconds"),
        Attribute("delayEnd", "Reverb delay range end", DoubleRange(0.0, 10.0), mDefaultDelayEnd, "seconds"),
        Attribute("power", "Repeat power", DoubleRange(0.0, 1.0), mDefaultPower.toDouble(), ""),
        Attribute("combFiltersAmount", "Repeats amount", DoubleRange(0.0, 10.0), mDefaultCombFiltersAmount, "")
    )
    override val effectName = "Reverb effect"

    override fun effectFinished() {
        mReverbBuffer = null
        mReverbOffsets = null
    }

    override fun checkAttributes(audioMiddleman : AudioMiddleman?): Boolean {
        val delayStart = attributesMap["delayStart"] ?: mDefaultDelayStart
        val delayEnd = attributesMap["delayEnd"] ?: mDefaultDelayEnd
        if (delayStart >= delayEnd) {
            audioMiddleman?.operationFinished(false, "Starting delay must be lower than the finish volume!", IllegalArgumentException("Starting delay must be lower than the finish volume!"))
            return false
        }
        return true
    }

    override fun applyEffect(audioBuffer: Array<FloatArray>, effectOutcome : Array<FloatArray>, audioInfo : AudioInfo, framesToAlter : Int) {
        if (mReverbBuffer == null || mReverbOffsets == null) {
            val combFiltersAmount = attributesMap["combFiltersAmount"]?.toInt() ?: mDefaultCombFiltersAmount.toInt()
            val delayStart = (attributesMap["delayStart"] ?: mDefaultDelayStart) * audioInfo.samplingRate
            val delayEnd = (attributesMap["delayEnd"] ?: mDefaultDelayEnd) * audioInfo.samplingRate
            mReverbBuffer = mutableListOf()
            mReverbOffsets = mutableListOf()
            for (bufferIndex in 0 until combFiltersAmount) {
                val reverbBufferSize = Random.nextInt(delayStart.toInt(), delayEnd.toInt())
                mReverbBuffer!!.add(Array(audioBuffer.size) { FloatArray(reverbBufferSize) { 0f } })
                mReverbOffsets!!.add(IntArray(audioBuffer.size) { 0 })
            }
        }
        val power = attributesMap["power"]?.toFloat() ?: mDefaultPower
        for (channelIndex in audioBuffer.indices) {
            var bufferOffset = 0
            while (bufferOffset < framesToAlter) {
                var summedBufferValues = 0.0f
                for (combFilterIndex in 0 until mReverbBuffer!!.size)
                    summedBufferValues += mReverbBuffer!![combFilterIndex][channelIndex][mReverbOffsets!![combFilterIndex][channelIndex]]
                effectOutcome[channelIndex][bufferOffset] = audioBuffer[channelIndex][bufferOffset] + summedBufferValues
                if (effectOutcome[channelIndex][bufferOffset] > 1) effectOutcome[channelIndex][bufferOffset] = 1f
                if (effectOutcome[channelIndex][bufferOffset] < -1) effectOutcome[channelIndex][bufferOffset] = -1f
                for (combFilterIndex in 0 until mReverbBuffer!!.size) {
                    mReverbBuffer!![combFilterIndex][channelIndex][mReverbOffsets!![combFilterIndex][channelIndex]] = power * audioBuffer[channelIndex][bufferOffset]
                    mReverbOffsets!![combFilterIndex][channelIndex] = (mReverbOffsets!![combFilterIndex][channelIndex] + 1) % mReverbBuffer!![combFilterIndex][0].size
                }
                bufferOffset++
            }
        }
    }

}