package effects

import com.adalbert.soundmash.connections.Attribute
import com.adalbert.soundmash.effects.AudioEffect
import com.adalbert.soundmash.effects.EffectGestureListener
import com.adalbert.soundmash.connections.AudioMiddleman
import com.adalbert.soundmash.connections.AudioInfo
import com.adalbert.soundmash.utils.DoubleRange

class CombFilter : AudioEffect() {

    private val mDefaultDelayFB = 0.25
    private val mDefaultDecayFB = 0.3
    private val mDefaultDelayFF = 0.4
    private val mDefaultDecayFF = 0.0
    override val effectGestureListener: EffectGestureListener? = null

    override val attributesDescription = listOf(
        Attribute("delayFeedback", "Delay of repeating echo", DoubleRange(0.0, 10.0), mDefaultDelayFB, "s"),
        Attribute("decayFeedback", "Decay of repeating echo", DoubleRange(0.0, 1.0), mDefaultDecayFB, ""),
        Attribute("delayFeedforward", "Delay of single echo", DoubleRange(0.0, 10.0), mDefaultDelayFF, "s"),
        Attribute("decayFeedforward", "Decay of single echo", DoubleRange(0.0, 1.0), mDefaultDecayFF, "")
    )

    override val effectName: String = "Echo effect"

    override fun checkAttributes(audioMiddleman: AudioMiddleman?): Boolean = true

    private var mBufferFeedback : Array<FloatArray>? = null
    private var mBufferFeedforward : Array<FloatArray>? = null
    private var mFeedbackIndices : IntArray? = null
    private var mFeedforwardIndices : IntArray? = null
    private var decayFeedback : Float = mDefaultDecayFB.toFloat()
    private var decayFeedforward : Float = mDefaultDecayFF.toFloat()

    override fun effectFinished() {
        mBufferFeedback = null
    }

    override fun applyEffect(audioBuffer: Array<FloatArray>, effectOutcome: Array<FloatArray>, audioInfo: AudioInfo, framesToAlter: Int) {
        if (mBufferFeedback == null) {
            val delayFeedback: Int = ((attributesMap["delayFeedback"] ?: mDefaultDelayFB) * audioInfo.samplingRate).toInt()
            decayFeedback = (attributesMap["decayFeedback"] ?: mDefaultDecayFB).toFloat()
            mBufferFeedback = Array(audioBuffer.size) { FloatArray(delayFeedback) { 0.0f } }
            mFeedbackIndices =  IntArray(audioBuffer.size) { 0 }
            val delayFeedforward: Int = ((attributesMap["delayFeedforward"] ?: mDefaultDelayFF) * audioInfo.samplingRate).toInt()
            decayFeedforward = (attributesMap["decayFeedforward"] ?: mDefaultDecayFF).toFloat()
            mBufferFeedforward = Array(audioBuffer.size) { FloatArray(delayFeedforward) { 0.0f } }
            mFeedforwardIndices =  IntArray(audioBuffer.size) { 0 }
        }

        var channelIndex = 0
        while (channelIndex < audioBuffer.size) {
            var sampleIndex = 0
            while (sampleIndex < audioBuffer[channelIndex].size) {
                val feedbackIndex = mFeedbackIndices!![channelIndex] % mBufferFeedback!![channelIndex].size
                val feedforwardIndex = mFeedforwardIndices!![channelIndex] % mBufferFeedforward!![channelIndex].size
                val feedbackValue =  decayFeedback * mBufferFeedback!![channelIndex][feedbackIndex]
                val feedforwardValue  = decayFeedforward * mBufferFeedforward!![channelIndex][feedforwardIndex]
                mBufferFeedforward!![channelIndex][feedforwardIndex] = audioBuffer[channelIndex][sampleIndex]
                effectOutcome[channelIndex][sampleIndex] = audioBuffer[channelIndex][sampleIndex] + feedforwardValue - feedbackValue
                mBufferFeedback!![channelIndex][feedbackIndex] = effectOutcome[channelIndex][sampleIndex]
                mFeedbackIndices!![channelIndex]++
                mFeedforwardIndices!![channelIndex]++
                sampleIndex++
            }
            channelIndex++
        }
    }
}