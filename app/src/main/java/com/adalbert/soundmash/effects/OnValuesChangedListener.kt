package com.adalbert.soundmash.effects

import com.adalbert.soundmash.connections.Attribute

/**
 * The interface describes an object, that is informed by a connected [EffectGestureListener] of its changes
 */
interface OnValuesChangedListener {
    /**
     * Informs object of values changes
     * @param valuesChanged List of [Attribute] objects, that were changed within connected [EffectGestureListener]
     */
    fun onValuesChanged(valuesChanged : List<Attribute>)
}