package com.adalbert.soundmash.effects.effectsImplementations

import com.adalbert.soundmash.connections.Attribute
import com.adalbert.soundmash.effects.AudioEffect
import com.adalbert.soundmash.effects.EffectGestureListener
import com.adalbert.soundmash.connections.AudioMiddleman
import com.adalbert.soundmash.connections.AudioInfo
import com.adalbert.soundmash.utils.DoubleRange
import kotlin.math.exp

class DistortionEffect : AudioEffect() {

    private val mDefaultGain = 10.0
    private val mDefaultCorrection = 0.4

    override val effectGestureListener: EffectGestureListener? = null

    override val attributesDescription = listOf(
        Attribute("correction", "Correction", DoubleRange(0.0, 1.0), mDefaultCorrection, ""),
        Attribute("gain", "Gain", DoubleRange(0.0, 100.0), mDefaultGain, "")
    )

    override val effectName: String = "Distortion effect"

    override fun checkAttributes(audioMiddleman: AudioMiddleman?): Boolean = true
    override fun effectFinished() {}

    override fun applyEffect(audioBuffer: Array<FloatArray>, effectOutcome: Array<FloatArray>, audioInfo: AudioInfo, framesToAlter: Int) {
        val correction = (attributesMap["correction"] ?: mDefaultCorrection).toFloat()
        val gain = (attributesMap["gain"] ?: mDefaultGain).toFloat()
        var channelIndex = 0
        while (channelIndex < audioBuffer.size) {
            var sampleIndex = 0
            while (sampleIndex < audioBuffer[channelIndex].size) {
                effectOutcome[channelIndex][sampleIndex] = when {
                    audioBuffer[channelIndex][sampleIndex] > 0 -> 1f - exp(-gain * audioBuffer[channelIndex][sampleIndex])
                    else -> -1f +  exp(gain * audioBuffer[channelIndex][sampleIndex])
                }
                effectOutcome[channelIndex][sampleIndex] *= correction
                sampleIndex++
            }
            channelIndex++
        }
    }
}