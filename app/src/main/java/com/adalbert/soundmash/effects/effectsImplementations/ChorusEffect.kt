package com.adalbert.soundmash.effects.effectsImplementations

import com.adalbert.soundmash.connections.Attribute
import com.adalbert.soundmash.effects.AudioEffect
import com.adalbert.soundmash.effects.EffectGestureListener
import com.adalbert.soundmash.connections.AudioInfo
import com.adalbert.soundmash.connections.AudioMiddleman
import com.adalbert.soundmash.utils.DoubleRange
import java.lang.IllegalArgumentException

class ChorusEffect : AudioEffect() {

    private val mDefaultVariationStart = 40.0
    private val mDefaultVariationEnd = 60.0
    private val mDefaultVariationFreq = 0.25

    override val effectGestureListener: EffectGestureListener? = null

    override val attributesDescription = listOf(
        Attribute(
            "variationStart", "Lower bound for the delay",
            DoubleRange(0.0, 1000.0), mDefaultVariationStart, "ms"
        ),
        Attribute(
            "variationEnd", "Upper bound for the delay",
            DoubleRange(0.0, 1000.0), mDefaultVariationEnd, "ms"
        ),
        Attribute(
            "variationFreq", "Frequency of delay changes",
            DoubleRange(0.0, 3.0), mDefaultVariationFreq, "Hz"
        )
    )
    override val effectName = "Chorus effect"

    private var mChorusBuffer : Array<FloatArray>? = null
    // Needs to be processed by indexModulo too, because it serves as a global indexer outside of the buffer domains
    private var mChorusYoungest : Array<Int>? = null
    private var mChorusVariations : Array<Double>? = null
    private var mChorusRaisingStatus : Array<Boolean>? = null
    private var mVariationEnd : Double? = null
    private var mVariationStart : Double? = null
    private var mVariationFreq : Double? = null

    override fun effectFinished() {
        mChorusBuffer = null
        mChorusYoungest = null
        mChorusVariations = null
        mChorusRaisingStatus = null
        mVariationEnd = null
        mVariationStart = null
        mVariationFreq = null
    }

    override fun checkAttributes(audioMiddleman: AudioMiddleman?): Boolean {
        val variationStart = attributesMap["variationStart"] ?: mDefaultVariationEnd
        val variationEnd = attributesMap["variationEnd"] ?: mDefaultVariationEnd
        if (variationStart >= variationEnd) {
            audioMiddleman?.operationFinished(false, "Starting variation must be lower than the finish volume!", IllegalArgumentException("Starting variation must be lower than the finish volume!"))
            return false
        }
        return variationEnd > variationStart
    }

    private fun indexModulo(index : Int) = ((index % mChorusBuffer!![0].size) + mChorusBuffer!![0].size) % mChorusBuffer!![0].size

    private fun delayFunction(channelIndex : Int, sampleIndex : Int) : Double {
        val variationDelta = (mVariationEnd!! - mVariationStart!!) / mVariationFreq!!
        return when (mChorusYoungest!![channelIndex] < mVariationEnd!!) {
            true -> 0.0
            false -> {
                mChorusVariations!![channelIndex] += when (mChorusRaisingStatus!![channelIndex]) {
                    true -> variationDelta
                    else -> -variationDelta
                }
                if (mChorusVariations!![channelIndex] >= mVariationEnd!!.toInt() || mChorusVariations!![channelIndex] <= mVariationStart!!.toInt())
                    mChorusRaisingStatus!![channelIndex] = !mChorusRaisingStatus!![channelIndex]

                mChorusVariations!![channelIndex]
            }
        }
    }

    private var bufferCounter = 0

    override fun applyEffect(audioBuffer: Array<FloatArray>, effectOutcome: Array<FloatArray>, audioInfo : AudioInfo, framesToAlter: Int) {
        if (mChorusBuffer == null || mChorusYoungest == null || mChorusRaisingStatus == null || mChorusVariations == null ||
            mVariationEnd == null || mVariationStart == null || mVariationFreq == null) {
            mVariationStart = (attributesMap["variationStart"] ?: mDefaultVariationEnd) / 1000 * audioInfo.samplingRate
            mVariationEnd = (attributesMap["variationEnd"] ?: mDefaultVariationEnd) / 1000 * audioInfo.samplingRate
            mVariationFreq = (1 / (attributesMap["variationFreq"] ?: mDefaultVariationFreq)) * audioInfo.samplingRate
            mChorusBuffer = Array(audioBuffer.size) { FloatArray(mVariationEnd!!.toInt()) { 0f } }
            mChorusYoungest = Array(audioBuffer.size) { 0 }
            mChorusVariations = Array(audioBuffer.size) { mVariationStart!! }
            mChorusRaisingStatus = Array(audioBuffer.size) { true }
        }
        for (channelIndex in audioBuffer.indices) {
            var bufferOffset = 0
            while (bufferOffset < framesToAlter) {
                val chorusIndex = indexModulo((indexModulo(mChorusYoungest!![channelIndex]) - delayFunction(channelIndex, mChorusYoungest!![channelIndex])).toInt())
                effectOutcome[channelIndex][bufferOffset] = audioBuffer[channelIndex][bufferOffset] + mChorusBuffer!![channelIndex][chorusIndex]
                if (effectOutcome[channelIndex][bufferOffset] > 1) effectOutcome[channelIndex][bufferOffset] = 1f
                if (effectOutcome[channelIndex][bufferOffset] < -1) effectOutcome[channelIndex][bufferOffset] = -1f
                mChorusBuffer!![channelIndex][indexModulo(mChorusYoungest!![channelIndex])] = audioBuffer[channelIndex][bufferOffset]
                mChorusYoungest!![channelIndex]++
                bufferOffset++
            }
        }
        bufferCounter++

    }
}