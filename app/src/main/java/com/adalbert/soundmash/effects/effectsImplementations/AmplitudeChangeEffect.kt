package com.adalbert.soundmash.effects.effectsImplementations

import com.adalbert.soundmash.connections.Attribute
import com.adalbert.soundmash.effects.AudioEffect
import com.adalbert.soundmash.utils.DoubleRange
import com.adalbert.soundmash.effects.effectsTouchListeners.AmplitudeChangeGestureListener
import com.adalbert.soundmash.connections.AudioInfo
import com.adalbert.soundmash.connections.AudioMiddleman
import kotlin.math.max
import kotlin.math.min

class AmplitudeChangeEffect : AudioEffect() {

    // Bizzare, it has to be before effectGestureListener because of its initialization
    override val attributesDescription: List<Attribute> = listOf(
        Attribute("power", "Outcome power", DoubleRange(0.0, 5.0), 1.0, "")
    )
    override val effectGestureListener: AmplitudeChangeGestureListener = AmplitudeChangeGestureListener(attributesMap)
    override val effectName: String = "Amplitude change"

    override fun checkAttributes(audioMiddleman: AudioMiddleman?): Boolean = true

    override fun effectFinished() {}

    override fun applyEffect(audioBuffer: Array<FloatArray>, effectOutcome: Array<FloatArray>, audioInfo : AudioInfo, framesToAlter: Int) {
        for (channelIndex in audioBuffer.indices)
            for (sampleIndex in audioBuffer[channelIndex].indices)
                effectOutcome[channelIndex][sampleIndex] = max(min(audioBuffer[channelIndex][sampleIndex] * (attributesMap["power"]?.toFloat() ?: attributesDescription[0].defaultValue.toFloat()), 1.0f), -1.0f)
    }
}