package com.adalbert.soundmash.effects

import android.util.Log
import com.adalbert.soundmash.connections.AudioDestination
import com.adalbert.soundmash.connections.AudioInfo
import com.adalbert.soundmash.connections.Attribute
import com.adalbert.soundmash.connections.AudioMiddleman
import com.adalbert.soundmash.connections.AudioSource

/**
 * The abstract class is used to describe an audio source - an object, which can deliver audio data.
 */
abstract class AudioEffect {

    /**
     * This property describes the effect's parameters map.
     * If a map already exists, then it is returned.
     * If not, then it is created, using [attributesDescription].
     */
    private var mAttributesMap : MutableMap<String, Double>? = null
    val attributesMap : MutableMap<String, Double>
        get() {
            mAttributesMap = mAttributesMap ?: {
                val attributesMap = mutableMapOf<String, Double>()
                for (attribute in attributesDescription)
                    attributesMap[attribute.attributeID] = attribute.defaultValue
                attributesMap
            }()
            return mAttributesMap!!
        }

    /**
     * An optional property, responsible for the connection between [EffectGestureListener] and [AudioEffect].
     */
    abstract val effectGestureListener : EffectGestureListener?

    /**
     * A list, containing descriptions of effect's attributes.
     */
    abstract val attributesDescription : List<Attribute>

    abstract val effectName : String

    /**
     * Streams data to effect implementation,
     * @param audioSource Audio source used for data delivery
     * @param [audioDestination] Audio destination used for data output
     * @param [audioMiddleman] Audio middleman used for status reporting
     * @param offset Describes, what point of the audio source take audio data from
     * @param framesToAlter Describes, how many frames from the audio source are to be processed
     */
    fun streamDataToEffect(audioSource : AudioSource, audioDestination: AudioDestination, audioMiddleman: AudioMiddleman?, offset : Int, framesToAlter : Int) {
        val startTime = System.currentTimeMillis()
        if (!checkAttributes(audioMiddleman))
            return
        val bufferSize = 4096
        val audioBuffer = Array(audioSource.audioInfo.numChannels) { FloatArray(bufferSize)}
        val effectOutcome = Array(audioSource.audioInfo.numChannels) { FloatArray(bufferSize)}
        audioSource.openSource()
        audioDestination.openDestination()
        var loadedFrames = 0
        while (loadedFrames < framesToAlter) {
            val framesToLoad = when (loadedFrames + bufferSize < framesToAlter) {
                true -> bufferSize
                false -> framesToAlter - loadedFrames
            }
            audioSource.loadBuffer(audioBuffer, offset + loadedFrames, framesToLoad)
            applyEffect(audioBuffer, effectOutcome, audioSource.audioInfo, framesToLoad)
            audioDestination.saveBuffer(effectOutcome,audioSource.audioInfo, offset + loadedFrames, framesToLoad)
            loadedFrames += framesToLoad
        }
        Log.d("EFFECT_FINISHED", "Effect finished after ${System.currentTimeMillis() - startTime} ms")
        effectFinished()
        audioDestination.notifyFinishOperation()
        audioSource.closeSource()
        audioDestination.closeDestination()
    }

    /**
     * Checks, if attributes values in [attributesMap] match the effect's constraints
     * @param audioMiddleman Used for error reporting
     */
    protected abstract fun checkAttributes(audioMiddleman: AudioMiddleman?) : Boolean

    /**
     * Clears internal state of the effect or filter, if needed
     */
    protected abstract fun effectFinished()

    /**
     * Applies transformation for singular audio buffer
     * @param audioBuffer Multi-channel buffer for storing incoming audio information
     * @param effectOutcome Multi-channel buffer for storing returned audio information
     * @param audioInfo Information about audio parameters, see [AudioInfo] docs for description
     * @param framesToAlter Describes, how many frames from the incoming data are to be processed
     */
    abstract fun applyEffect(audioBuffer : Array<FloatArray>, effectOutcome : Array<FloatArray>, audioInfo : AudioInfo, framesToAlter: Int)

}