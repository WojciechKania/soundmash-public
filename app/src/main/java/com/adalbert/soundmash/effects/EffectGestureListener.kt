package com.adalbert.soundmash.effects

import com.adalbert.soundmash.gestures.CompleteGestureListener

/**
 * The abstract class describes a gesture listener appointed to an effect.
 * @property attributesMap Attributes map of the modified effect, so the changes can be applied directly to it
 */
abstract class EffectGestureListener(protected val attributesMap : MutableMap<String, Double>) : CompleteGestureListener {
    /**
     * Adds a listener, that is informed about effect gesture listener activation
     */
    abstract fun addOnValuesChangedListener(listener: OnValuesChangedListener)

    /**
     * Removes a listener, that is informed about effect gesture listener activation
     */
    abstract fun removeOnValuesChangedListener(listener : OnValuesChangedListener)
}