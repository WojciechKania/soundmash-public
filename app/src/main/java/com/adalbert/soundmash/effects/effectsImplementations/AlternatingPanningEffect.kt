package com.adalbert.soundmash.effects.effectsImplementations

import com.adalbert.soundmash.connections.Attribute
import com.adalbert.soundmash.effects.AudioEffect
import com.adalbert.soundmash.effects.EffectGestureListener
import com.adalbert.soundmash.connections.AudioInfo
import com.adalbert.soundmash.connections.AudioMiddleman
import com.adalbert.soundmash.utils.DoubleRange
import java.lang.IllegalArgumentException

class AlternatingPanningEffect : AudioEffect() {

    override val effectGestureListener: EffectGestureListener? = null

    private val mDefaultPeriod = 1.0
    private val mDefaultVolumeStart = 0.0
    private val mDefaultVolumeEnd = 1.0

    override val attributesDescription = listOf(
        Attribute(
            "volumeStart", "Volume range start",
            DoubleRange(0.0, 1.0), mDefaultVolumeStart, ""
        ),
        Attribute(
            "volumeEnd", "Volume range finish",
            DoubleRange(0.0, 1.0), mDefaultVolumeEnd, ""
        ),
        Attribute(
            "period", "Period",
            DoubleRange(0.0, 60.0), mDefaultPeriod, "seconds"
        )
    )
    override val effectName: String
        get() = "Alternating Panning effect"

    private val volumeRange = DoubleRange(0.0, 1.0)

    private var differenceLeft : Double? = null
    private var differenceRight : Double? = null
    private var volumeStart : Double? = null
    private var volumeEnd : Double? = null
    private var volumeLeft : Double? = null
    private var volumeRight : Double? = null

    override fun effectFinished() {
        differenceLeft = null
        differenceRight = null
    }

    override fun checkAttributes(audioMiddleman: AudioMiddleman?): Boolean {
        val volumeEnd = attributesMap["volumeEnd"] ?: mDefaultVolumeEnd
        val volumeStart = attributesMap["volumeStart"] ?: mDefaultVolumeStart
        if (volumeStart >= volumeEnd) {
            audioMiddleman?.operationFinished(false, "Starting volume must be lower than the finish volume!", IllegalArgumentException("Starting volume must be lower than the finish volume!"))
            return false
        }
        return true
    }

    override fun applyEffect(audioBuffer: Array<FloatArray>, effectOutcome: Array<FloatArray>, audioInfo : AudioInfo, framesToAlter: Int) {
        if (differenceLeft == null || differenceRight == null) {
            val period = attributesMap["period"]?.times(audioInfo.samplingRate) ?: mDefaultPeriod
            volumeStart = attributesMap["volumeStart"] ?: mDefaultVolumeStart
            volumeEnd = attributesMap["volumeEnd"] ?: mDefaultVolumeEnd
            volumeLeft = volumeStart
            volumeRight = volumeEnd
            differenceLeft = 2 * (volumeEnd!! - volumeStart!!) / period
            differenceRight = 2 * (volumeEnd!! - volumeStart!!) / period
        }
        for (channelIndex in audioBuffer.indices) {
            for (sampleIndex in audioBuffer[channelIndex].indices) {
                val volume = when (channelIndex % 2 == 0) {
                    true -> {
                        if (volumeLeft!! + differenceLeft!! > volumeEnd!! || volumeLeft!! + differenceLeft!! < volumeStart!!)
                            differenceLeft = differenceLeft!! * -1.0
                        volumeLeft = volumeLeft!! + differenceLeft!!
                        volumeRange.cutTo(volumeLeft!!)
                    }
                    false -> {
                        if (volumeRight!! + differenceRight!! > volumeEnd!! || volumeRight!! + differenceRight!! < volumeStart!!)
                            differenceRight = differenceRight!! * -1.0
                        volumeRight = volumeRight!! + differenceRight!!
                        volumeRange.cutTo(volumeRight!!)
                    }
                }
                effectOutcome[channelIndex][sampleIndex] = audioBuffer[channelIndex][sampleIndex] * volume.toFloat()
            }
        }
    }
}