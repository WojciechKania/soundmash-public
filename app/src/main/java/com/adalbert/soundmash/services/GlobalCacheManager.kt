package com.adalbert.soundmash.services

import android.util.Log
import java.io.File

object GlobalCacheManager {

    // TODO: Move that to future settings
    private const val cacheLimit = 200 * 1024 * 1024

    private val mutex = Object()
    private var mNextIndex = 0L
    private val mRegisteredFiles = mutableListOf<File>()

    private var mExternalCacheDir : File? = null

    /**
     * Place appointed by the operating system, where the application can keep data.
     */
    var externalCacheDir : File
        get() = mExternalCacheDir!!
        set(value) {
            if (mExternalCacheDir == null)
                mExternalCacheDir = value
        }

    /**
     * @return The size of currently registered cache files.
     */
    fun currentCacheSize() : Long {
        var currentCacheSize = 0L
        mRegisteredFiles.forEach { currentCacheSize += it.length() }
        return currentCacheSize
    }

    /**
     * @return Indicator, if the current cache size overflows the allowed size.
     */
    fun isCacheOverloaded(additionalSpace : Long = 0) : Boolean {
        return (currentCacheSize() + additionalSpace) > cacheLimit
    }

    /**
     * Helper method for clearing all the cache files
     */
    @Synchronized fun clearCache() {
        mRegisteredFiles.forEach { it.delete() }
    }

    /**
     * Registers file for usage as a cache file.
     * @return Registered file
     */
    @Synchronized fun registerFile() : File {
        Log.d("FILE_REGISTERED", "File $mNextIndex.wav is being registered")
        val returnValue = "${externalCacheDir.absolutePath}/${mNextIndex++}"
        mRegisteredFiles.add(File("$returnValue.wav"))
        return File(returnValue)
    }

    /**
     * Unregisters the most distantly registered file.
     */
    @Synchronized fun unregisterLastFile() {
        if (mRegisteredFiles.isNotEmpty()) {
            Log.d("FILE_UNREGISTERED", "File ${mRegisteredFiles.first().name} is being unregistered")
            mRegisteredFiles.first().delete()
            mRegisteredFiles.removeAt(0)
        } else
            Log.e("FILE_UNREGISTERED_FAILURE", "The next file can't be unregistered, it doesn't exist!")
    }

}