package com.adalbert.soundmash.services

import android.media.AudioAttributes
import android.media.AudioTrack
import com.adalbert.soundmash.models.Track


// TODO : Make a legit service out of it
class TrackPlayer {

    private var mPlayingThread: Thread? = null
    private lateinit var mTrack : Track

    private var mIsInterrupted = false
    private var mBeginIndex : Int = 0
    private var mFinishIndex : Int = 0

    private val mAllowedTimeToSleep = 500

    private val playingRunnable = Runnable {
        if (mTrack.numChannels == 0 || mTrack.numFrames == 0 || mTrack.samplingRate == 0)
            return@Runnable
        val channelConfig = when (mTrack.numChannels) {
            1 -> android.media.AudioFormat.CHANNEL_OUT_MONO
            2 -> android.media.AudioFormat.CHANNEL_OUT_STEREO
            else -> android.media.AudioFormat.CHANNEL_OUT_STEREO
        }
        var bufferSize = AudioTrack.getMinBufferSize(
            mTrack.samplingRate,
            channelConfig,
            android.media.AudioFormat.ENCODING_PCM_FLOAT
        )
        if (bufferSize == AudioTrack.ERROR || bufferSize == AudioTrack.ERROR_BAD_VALUE) {
            bufferSize = mTrack.samplingRate * mTrack.numChannels * 2
        }
        val audioTrack = AudioTrack.Builder()
            .setAudioAttributes(
                AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()
            )
            .setAudioFormat(
                android.media.AudioFormat.Builder()
                    .setEncoding(android.media.AudioFormat.ENCODING_PCM_FLOAT)
                    .setSampleRate(mTrack.samplingRate)
                    .setChannelMask(channelConfig)
                    .build()
            )
            .setBufferSizeInBytes(bufferSize)
            .build()
        audioTrack.play()
        val buffer = FloatArray(bufferSize)
        var framesWritten = mBeginIndex
        while (framesWritten < mFinishIndex && !mIsInterrupted) {
            val (endIndex: Int, samplesToWrite: Int) = when ((mFinishIndex - framesWritten) * mTrack.numChannels >= bufferSize) {
                true -> Pair(bufferSize / mTrack.numChannels + framesWritten, bufferSize)
                false -> Pair(mFinishIndex, (mFinishIndex - framesWritten) * mTrack.numChannels)
            }
            mTrack.getCopyOfSamplesInSingleArray(buffer, framesWritten, endIndex)
            audioTrack.write(buffer, 0, samplesToWrite, AudioTrack.WRITE_BLOCKING)
            framesWritten += samplesToWrite / mTrack.numChannels
        }
    }

    fun playSelection(track: Track, playBegin: Int = mBeginIndex, playEnd: Int = track.numFrames) {
        Thread {
            var timeSlept = 0
            mIsInterrupted = true
            while (mPlayingThread?.isAlive == true) {
                timeSlept += 10
                Thread.sleep(10)
                if(timeSlept > mAllowedTimeToSleep) return@Thread
            }
            mIsInterrupted = false
            mTrack = track
            mBeginIndex = playBegin
            mFinishIndex = playEnd
            mPlayingThread = Thread(playingRunnable)
            mPlayingThread!!.start()
        }.start()
    }

    fun stopPlaying() {
        Thread {
            var timeSlept = 0
            mIsInterrupted = true
            while (mPlayingThread?.isAlive == true) {
                timeSlept += 10
                Thread.sleep(10)
                if(timeSlept > mAllowedTimeToSleep) return@Thread
            }
            mIsInterrupted = false
        }.start()
        mBeginIndex = 0
    }
}