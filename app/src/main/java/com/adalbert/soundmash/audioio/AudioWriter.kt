package com.adalbert.soundmash.audioio

import android.content.Context
import android.util.Log
import cafe.adriel.androidaudioconverter.AndroidAudioConverter
import cafe.adriel.androidaudioconverter.callback.IConvertCallback
import cafe.adriel.androidaudioconverter.model.AudioFormat
import com.adalbert.soundmash.connections.AudioMiddleman
import com.adalbert.soundmash.connections.AudioSource
import edu.illinois.cs.cs125.lib.wavfile.WavFile
import java.io.File
import java.lang.Exception
import java.util.*

class AudioWriter private constructor(private val appContext: Context) {

    private val mOrdersQueue: MutableList<WriteOrder> = Collections.synchronizedList(mutableListOf())

    @get:Synchronized @set:Synchronized
    private var isThreadAlive = false

    companion object {
        private var instance : AudioWriter? = null

        fun getInstance(appContext: Context) : AudioWriter {
            if (instance == null)
                instance = AudioWriter(appContext)
            return instance!!
        }
    }

    private val handleOrders = Runnable {
        while (mOrdersQueue.size > 0) {
            val writeOrder = mOrdersQueue.removeAt(0)
            val audioFormat = writeOrder.format
            if (audioFormat == AudioFormat.WAV) {
                val file = File("${writeOrder.filePath}.wav")
                val result = writeToNewWav(file, writeOrder.audioSource, writeOrder.audioMiddleman, writeOrder.offset, writeOrder.framesToWrite)
                if (result) {
                    writeOrder.audioMiddleman.operationFinished(true)
                    return@Runnable
                }
            } else if (AudioIO.isFFMPEGSupported) {
                val tempFile = File("${writeOrder.filePath}.wav")
                val result = writeToNewWav(tempFile, writeOrder.audioSource, writeOrder.audioMiddleman,
                   writeOrder.offset, writeOrder.framesToWrite
                )
                if (result) {
                    convertToGivenFormat(tempFile, audioFormat, object : IConvertCallback {
                        override fun onSuccess(convertedFile: File?) {
                            tempFile.delete()
                            convertedFile?.let {
                                writeOrder.audioMiddleman.operationFinished(true)
                                return
                            }
                            writeOrder.audioMiddleman.operationFinished(false, "Couldn't convert the file to specified format!", IllegalStateException())
                        }
                        override fun onFailure(error: Exception?) {
                            tempFile.delete()
                            writeOrder.audioMiddleman.operationFinished(false, "Couldn't convert the file to specified format!", error)
                        }
                    })
                    return@Runnable
                }
            } else {
                writeOrder.audioMiddleman.operationFinished(false, "Audio conversion is not supported on your system!", IllegalStateException())
                return@Runnable
            }
            writeOrder.audioMiddleman.operationFinished(false, "Internal error while writing file!", IllegalStateException())
        }
    }

    // TODO : Exchange -1 for null
    fun writeFrames(filePath: String, format: AudioFormat,
                    audioSource : AudioSource, audioMiddleman: AudioMiddleman,
                    offset: Int = 0, framesToWrite: Int = -1
    ) {
        val writeOrder = WriteOrder(filePath, format, audioSource, audioMiddleman, offset, framesToWrite)
        mOrdersQueue.add(writeOrder)
        if (!isThreadAlive) {
            Thread {
                isThreadAlive = true
                handleOrders.run()
                isThreadAlive = false
            }.start()
        }
    }

    private fun writeToNewWav(file: File, audioSource: AudioSource, audioMiddleman: AudioMiddleman, offset: Int, framesToWrite: Int) : Boolean {

        // Sanity check
        if (audioSource.numFrames == 0) {
            audioMiddleman.operationFinished(false,"There is no data to save!", ArrayIndexOutOfBoundsException())
            return false
        }

        val startTime = System.currentTimeMillis()
        val dataSize = when (framesToWrite) {
            -1 -> audioSource.numFrames
            else -> framesToWrite
        }
        try {
            val audioInfo = audioSource.audioInfo
            val wavFile = WavFile.newWavFile(file, audioInfo.numChannels, dataSize.toLong(), audioInfo.validBits, audioInfo.samplingRate.toLong())
            val bufferSize = 4096
            val audioBuffer = Array(audioInfo.numChannels) { FloatArray(bufferSize) }
            var loadedFrames = 0
            while (loadedFrames < dataSize) {
                val framesToLoad = when (loadedFrames + bufferSize < dataSize) {
                    true -> bufferSize
                    false -> dataSize - loadedFrames
                }
                audioSource.loadBuffer(audioBuffer, offset + loadedFrames, framesToLoad)
                wavFile.writeFrames(audioBuffer, framesToLoad)
                loadedFrames += framesToLoad
            }
            wavFile.close()
        } catch (ex: Exception) {
            audioMiddleman.operationFinished(false,"Couldn't write given samples into a file!", ex)
            return false
        }
        Log.d("WAVFILE", "WAVFILE WRITTEN IN ${System.currentTimeMillis() - startTime} ms")
        Log.d("WAVFILE", "WAVFILE CLOSED")
        return true
    }

    private fun convertToGivenFormat(file : File, format : AudioFormat, callback : IConvertCallback) {
        AndroidAudioConverter.with(appContext)
            .setFile(file)
            .setFormat(format)
            .setCallback(callback)
            .convert()
    }

}