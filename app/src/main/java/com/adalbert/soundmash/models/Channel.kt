package com.adalbert.soundmash.models

import kotlin.math.max

class Channel {

    /**
     * An array containing audio information
     */
    private var mData : FloatArray = FloatArray(0) {0f}

    /**
     * Index of the next data write
     */
    private var mNextWriteIndex = 0

    /**
     * The logical collection size, often quite different from the physical one
     */
    val samplesSize : Int
        get() = mNextWriteIndex

    /**
     * Removes samples by index
     * @param startIndex The beginning of removal
     * @param endIndex The end of removal
     */
    fun removeSamples(startIndex: Int = 0, endIndex : Int = mNextWriteIndex) {
        mData.copyInto(mData, startIndex, endIndex, mNextWriteIndex)
        mNextWriteIndex -= (endIndex - startIndex)
    }

    // TODO : Bufferize
    /**
     * Moves existing samples to another point in the audio channel.
     * @param sourceStartIndex Starting index of data to move
     * @param sourceEndIndex Finishing index of data to move
     * @param destinationIndex Index to move data to
     */
    fun moveSamples(sourceStartIndex : Int, sourceEndIndex : Int, destinationIndex : Int) {
        val movedSamples = mData.copyOfRange(sourceStartIndex, sourceEndIndex)
        removeSamples(sourceStartIndex, sourceEndIndex)
        insertSamples(movedSamples, destinationIndex)
    }

    /**
     * Should return a copy of the audio information.
     * @param startIndex Indicates, from which index the copying should start
     * @param endIndex Indicates, to which index the copying should end
     */
    fun getCopyOfSamples(startIndex : Int = 0, endIndex : Int = mNextWriteIndex) : FloatArray
        = mData.sliceArray(startIndex until endIndex)

    /**
     * Should return a copy of the audio information.
     * @param buffer An audio buffer into which the data from the channel should be loaded
     * @param startIndex Indicates, from which index the copying should start
     * @param endIndex Indicates, to which index the copying should end
     */
    fun getCopyOfSamples(buffer : FloatArray, startIndex : Int = 0, endIndex : Int = mNextWriteIndex) {
        mData.copyInto(buffer, 0, startIndex, endIndex)
    }

    /**
     * Should load a copy of the audio information into an array, in order described by [channelIndex].
     * @param buffer An audio buffer into which the data from the channel should be loaded
     * @param channelIndex Index of this channel in relation to the others in the track
     * @param channelAmount Amount of channels in the corresponding track
     * @param startIndex Indicates, from which index the copying should start
     * @param endIndex Indicates, to which index the copying should end
     */
    fun getCopyOfSamplesInSingleArray(buffer : FloatArray, channelIndex : Int, channelAmount : Int, startIndex : Int = 0, endIndex : Int = mNextWriteIndex) : FloatArray {
        for (sampleIndex in startIndex until endIndex)
            buffer[(sampleIndex - startIndex) * channelAmount + channelIndex] = mData[sampleIndex]
        return buffer
    }

    /**
     * Should return original audio information without copying data.
     */
    fun getOriginalSamples() : FloatArray {
        return mData
    }

    /**
     * Replaces existing frames with the given values.
     * @param samples Buffer of incoming audio data
     * @param offset Beginning of replacement
     * @param samplesToReplace Amount of samples to replace
     */
    fun replaceSamples(samples: FloatArray, offset : Int = 0, samplesToReplace : Int = samples.size) {
        if (samplesToReplace + offset > mNextWriteIndex)
            throw java.lang.IllegalArgumentException("Out of index!")
        samples.copyInto(mData, offset, 0, samplesToReplace)
    }

    /**
     * Inserts the given samples to the track.
     * @param samples An array capable of holding audio signal information
     * @param startIndex Offset, points to the place of insertion
     * @param samplesToLoad Amount of samples that are going to be inserted
     */
    fun insertSamples(samples : FloatArray, startIndex : Int = mNextWriteIndex, samplesToLoad : Int = samples.size) {
        if (startIndex < 0)
            throw IllegalArgumentException("Starting index less than zero!")
        if (mData.size - samplesSize < samplesToLoad || mData.size < startIndex + samplesToLoad) {
            val additionalSize = max((samplesToLoad - (mData.size - mNextWriteIndex)),
                (startIndex + samplesToLoad - mData.size))
            val newData = FloatArray(mData.size + additionalSize)
            mNextWriteIndex += if (startIndex > mNextWriteIndex) {
                mData.copyInto(newData, 0, 0, mNextWriteIndex)
                newData.fill(0f, mNextWriteIndex, startIndex)
                samples.copyInto(newData, startIndex, 0, samplesToLoad)
                max(startIndex - mNextWriteIndex, 0) + samplesToLoad
            } else {
                mData.copyInto(newData, 0, 0, startIndex)
                samples.copyInto(newData, startIndex, 0, samplesToLoad)
                mData.copyInto(newData, startIndex + samplesToLoad, startIndex, mNextWriteIndex)
                samplesToLoad
            }
            mData = newData
        } else {
            if (startIndex < mNextWriteIndex)
                mData.copyInto(mData, startIndex + samplesToLoad, startIndex, mNextWriteIndex)
            samples.copyInto(mData, startIndex, 0, samplesToLoad)
            mNextWriteIndex += max(startIndex - mNextWriteIndex, 0) + samplesToLoad
        }
    }

    /**
     * Clears the existing audio information, replacing it with given samples.
     * @param samples Array of samples data, with which the original information should be replaced
     * @param samplesToLoad Amount of samples to load
     */
    fun packSamples(samples : FloatArray, samplesToLoad: Int) {
        clearChannel()
        mData = samples
        mNextWriteIndex = samplesToLoad
    }

    /**
     * Notifies channel about additional space needed, allows for data structure allocation suitable for incoming audio information.
     * @param additionalDataSize Number of additional samples to make room for
     */
    fun assureSufficientCapacity(additionalDataSize : Int) {
        if (mData.size - samplesSize >= additionalDataSize)
            return
        mData = mData.copyOf(mData.size + additionalDataSize - (mData.size - samplesSize))
    }

    /**
     * Used for metadata and data clearing
     */
    fun clearChannel() {
        mData = FloatArray(0) { 0f }
        mNextWriteIndex = 0
    }

}