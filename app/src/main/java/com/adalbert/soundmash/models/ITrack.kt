package com.adalbert.soundmash.models

/**
 * An interface describing the audio track, implementation of which should be capable of manipulation of data information.
 */
interface ITrack {

    /**
     * Number of frames available in the track.
     */
    val numFrames : Int

    /**
     * Sampling rate of the contained audio information.
     */
    val samplingRate : Int

    /**
     * Channels amount of the contained audio information.
     */
    val numChannels : Int

    /**
     * Bit depth of the contained audio information.
     */
    val validBits : Int

    /**
     * Inserts the given samples to the track.
     * @param channelSamples Multi-channel data structure capable of holding audio signal information
     * @param samplingRate Sampling rate of the loaded audio
     * @param validBits Bit depth of the loaded audio
     * @param index Offset, points to the place of insertion
     * @param samplesToLoad Amount of samples that are going to be inserted
     */
    fun insertSamples(channelSamples : Array<FloatArray>, samplingRate: Int, validBits : Int, index : Int = numFrames, samplesToLoad : Int = channelSamples[0].size)

    /**
     * Inserts the given samples to the track, without notifying track listeners.
     * For parameters description, see [insertSamples].
     */
    fun bulkInsertSamples(channelSamples : Array<FloatArray>, samplingRate: Int, validBits : Int, index : Int = numFrames, samplesToLoad : Int = channelSamples[0].size)

    fun packSamples(channelSamples : Array<FloatArray>, samplingRate: Int, validBits : Int, samplesToLoad: Int)

    /**
     * Removes the selected samples from the track.
     * @param startIndex The beginning of removal
     * @param endIndex The finishing point of removal
     */
    fun removeSamples(startIndex : Int = 0, endIndex : Int = numFrames)

    /**
     * Removes the selected samples from the track, without notifying track listeners.
     * For parameters description, see [removeSamples].
     */
    fun bulkRemoveSamples(startIndex : Int = 0, endIndex : Int = numFrames)

    /**
     * Replaces existing frames with the given values.
     * @param buffer Multi-channel buffer of incoming audio data
     * @param offset Beginning of replacement
     * @param framesToReplace Amount of frames to replace
     */
    fun replaceSamples(buffer : Array<FloatArray>, offset : Int = 0, framesToReplace : Int = buffer[0].size)

    /**
     * Moves existing frames to another point in the audio track.
     * @param sourceStartIndex Starting index of data to move
     * @param sourceEndIndex Finishing index of data to move
     * @param destinationIndex Index to move data to
     */
    fun moveSamples(sourceStartIndex : Int, sourceEndIndex : Int, destinationIndex : Int)

    /**
     * Should return original audio information without copying data.
     */
    fun getOriginalSamples() : Array<FloatArray>

    /**
     * Should return a copy of the audio information.
     * @param buffer An audio buffer into which the data from the track should be loaded
     * @param startIndex Indicates, from which index the copying should start
     * @param endIndex Indicates, to which index the copying should end
     */
    fun getCopyOfSamples(buffer : Array<FloatArray>, startIndex : Int = 0, endIndex : Int = numFrames)

    /**
     * Should return a copy of the audio information, but in an array flattened according to channels.
     * @param buffer An audio buffer into which the data from the track should be loaded
     * @param startIndex Indicates, from which index the copying should start
     * @param endIndex Indicates, to which index the copying should end
     */
    fun getCopyOfSamplesInSingleArray(buffer : FloatArray, startIndex: Int = 0, endIndex: Int = numFrames)

    /**
     * Notifies existing mechanism of audio storage of the size of incoming audio data.
     * @param additionalDataSize Number of additional frames to make room for
     * @param amountOfChannels Amount of channels the audio data consists of
     */
    fun assureSufficientCapacity(additionalDataSize : Int, amountOfChannels : Int)

    /**
     * Used for metadata and data clearing
     */
    fun clearTrack()

    /**
     * Registers a [TrackListener].
     * @param listener Listener to register
     */
    fun registerListener(listener: TrackListener)

    /**
     * Unregisters a [TrackListener].
     * @param listener Listener to unregister
     */
    fun unregisterListener(listener: TrackListener)

    /**
     * Manually updates track listeners.
     */
    fun updateTrackChangesListeners()
}