package com.adalbert.soundmash.utils

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import kotlin.math.max
import kotlin.math.min

class DoubleRange(private var begin: Number, private var end: Number) {

    init {
        if (begin.toDouble() > end.toDouble()) {
            val temp = end.toDouble()
            end = begin.toDouble()
            begin = temp
        }
    }

    /**
     * Checks, if the given value is in the specified range, excluding the end of range.
     * @param value Value to be checked
     */
    fun isIn(value: Number) : Boolean {
        if (value.toDouble() >= begin.toDouble() && value.toDouble() < end.toDouble())
            return true
        return false
    }

    /**
     * Checks, if the given value is in the specified range, including the end of range.
     * @param value Value to be checked
     */
    fun isInIncluding(value: Number) : Boolean {
        if (value.toDouble() >= begin.toDouble() && value.toDouble() <= end.toDouble())
            return true
        return false
    }

    /**
     * If the value is in given range, returns it, otherwise return the range
     * beginning if value is smaller and the range end if value is bigger
     * @param value Value to be checked
     */
    fun cutTo(value: Number) : Double {
        return min(max(value.toDouble(), begin.toDouble()), end.toDouble())
    }

    override fun toString(): String {
        val symbols = DecimalFormatSymbols(Locale.US)
        symbols.decimalSeparator = '.'
        val format = DecimalFormat("#0.##", symbols)
        return "(${format.format(begin)} - ${format.format(end)})"
    }
}