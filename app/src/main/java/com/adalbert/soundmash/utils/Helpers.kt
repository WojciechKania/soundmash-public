package com.adalbert.soundmash.utils

import cafe.adriel.androidaudioconverter.model.AudioFormat
import java.util.*

val sSupportedAudioFormats : Sequence<String> = AudioFormat.values().asSequence().map { it.toString() }

fun getFileType(url : String): String {
    return url.substring(url.lastIndexOf('.') + 1).toUpperCase(Locale.ROOT)
}

