package com.adalbert.soundmash.connections.connectionsImplementations

import com.adalbert.soundmash.connections.AudioInfo
import com.adalbert.soundmash.connections.AudioSource

class ArrayCopyAudioSource(private val mChannelsData : Array<FloatArray>, samplingRate : Int, validBits : Int, framesToLoad: Int) : AudioSource {

    override val audioInfo = AudioInfo(samplingRate, validBits, mChannelsData.size)
    override val numFrames = framesToLoad

    override fun openSource() {}
    override fun closeSource() {}

    override fun loadBuffer(audioBuffer: Array<FloatArray>, offset: Int, framesToLoad: Int) {
        for (channelIndex in mChannelsData.indices)
            mChannelsData[channelIndex].copyInto(audioBuffer[channelIndex], 0, offset, offset + framesToLoad)
    }
}