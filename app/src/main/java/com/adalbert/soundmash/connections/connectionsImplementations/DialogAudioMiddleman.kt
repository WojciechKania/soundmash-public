package com.adalbert.soundmash.connections.connectionsImplementations

import android.app.Activity
import android.app.AlertDialog
import android.util.Log
import com.adalbert.soundmash.R
import com.adalbert.soundmash.connections.AudioMiddleman

class DialogAudioMiddleman(private val context : Activity, private val onSuccess: () -> Unit, private val onFailure: () -> Unit) : AudioMiddleman {
    override fun operationFinished(success: Boolean, message: String?, throwable: Throwable?) {
        val builder = AlertDialog.Builder(context)
        when (success) {
            true -> {
                onSuccess()
            }
            false -> {
                Log.e("OPERATION_FAILURE", message, throwable)
                builder.setTitle("Error")
                    .setMessage(message)
                    .setIcon(R.drawable.error)
                context.runOnUiThread() { builder.create().show() }
                onFailure()
            }
        }
    }
}