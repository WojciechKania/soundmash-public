package com.adalbert.soundmash.connections

/**
 * Describes parameters of audio signal
 * @property samplingRate - the sampling rate of the signal
 * @property validBits - the bit depth of the signal
 * @property numChannels - the amount of encoded channels
 */
class AudioInfo(val samplingRate : Int, val validBits : Int, val numChannels : Int) {

    override fun equals(other: Any?): Boolean {
        if (other != null && other is AudioInfo)
            return (samplingRate == other.samplingRate &&
                    validBits == other.validBits &&
                    numChannels == other.numChannels)
        return false
    }

    override fun hashCode(): Int {
        var result = samplingRate
        result = 31 * result + validBits
        result = 31 * result + numChannels
        return result
    }

}