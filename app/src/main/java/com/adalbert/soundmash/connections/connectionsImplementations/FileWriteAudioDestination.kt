package com.adalbert.soundmash.connections.connectionsImplementations

import com.adalbert.soundmash.connections.AudioDestination
import com.adalbert.soundmash.connections.AudioInfo
import com.adalbert.soundmash.connections.AudioMiddleman
import edu.illinois.cs.cs125.lib.wavfile.WavFile
import java.io.File
import java.lang.IllegalStateException

class FileWriteAudioDestination(private val filePath: String, override val desiredAudioInfo: AudioInfo, private val mAudioMiddleman : AudioMiddleman?) : AudioDestination {

    private var wavFile : WavFile? = null

    override fun openDestination() {}

    override fun notifyDataSize(additionalSize: Int, numChannels: Int) {
        if (wavFile == null)
            wavFile = WavFile.newWavFile(File(filePath), numChannels, additionalSize.toLong(), desiredAudioInfo.validBits, desiredAudioInfo.samplingRate.toLong())
    }

    override fun saveBuffer(audioBuffer: Array<FloatArray>, actualAudioInfo: AudioInfo, offset: Int, framesToLoad: Int) {
        if (wavFile == null)
            mAudioMiddleman?.operationFinished(false, "There were problems with audio files!", IllegalStateException("FileWriteAudioDestination not initialized"))
        wavFile?.let {
            it.writeFrames(audioBuffer, framesToLoad)
        }
    }

    override fun notifyFinishOperation() {}

    override fun closeDestination() {
        wavFile?.let {
            it.close()
            wavFile = null
        }
    }
}