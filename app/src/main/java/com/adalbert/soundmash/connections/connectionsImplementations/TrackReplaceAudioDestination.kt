package com.adalbert.soundmash.connections.connectionsImplementations

import com.adalbert.soundmash.models.ITrack
import com.adalbert.soundmash.connections.AudioDestination
import com.adalbert.soundmash.connections.AudioInfo
import com.adalbert.soundmash.connections.AudioMiddleman

class TrackReplaceAudioDestination(private val mTrack : ITrack, private val mAudioMiddleman : AudioMiddleman?) : AudioDestination {

    override val desiredAudioInfo: AudioInfo = AudioInfo(mTrack.samplingRate, mTrack.validBits, mTrack.numChannels)

    override fun openDestination() {}
    override fun closeDestination() {}

    override fun saveBuffer(audioBuffer: Array<FloatArray>, actualAudioInfo : AudioInfo, offset: Int, framesToLoad: Int) {
        mTrack.replaceSamples(audioBuffer, offset, framesToLoad)
    }

    override fun notifyDataSize(additionalSize: Int, numChannels: Int) {
    }

    override fun notifyFinishOperation() {
        mAudioMiddleman?.operationFinished(true, null, null)
    }

}