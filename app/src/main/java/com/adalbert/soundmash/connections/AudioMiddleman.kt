package com.adalbert.soundmash.connections

/**
 * This interface is used to describe audio middleman - which can perform operations upon audio operation finish.
 */
interface AudioMiddleman {
    /**
     * This method should be called, when audio operation is finished.
     * Performs specific tasks, specified by the interface realization.
     * @param success Describes, if the operation was successful
     * @param message Contains optional message to the user
     * @param throwable Contains optional throwable for the caller
     */
    fun operationFinished(success : Boolean, message : String? = null, throwable : Throwable? = null)
}