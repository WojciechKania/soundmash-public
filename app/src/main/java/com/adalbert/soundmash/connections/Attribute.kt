package com.adalbert.soundmash.connections

import com.adalbert.soundmash.utils.DoubleRange

/**
 * Describes an attribute's value and contains some messages for the user
 * @property attributeID The ID given to the attribute, for programmer's convenience
 * @property message Short text description of the attribute's role, usually written for the user's convenience
 * @property range Range of values, which can be taken by an attribute
 * @property defaultValue Default value of given attribute
 * @property unit Unit of the parameter, usually writen for the user's convenience
 */
class Attribute(val attributeID: String, val message: String, val range: DoubleRange, val defaultValue: Double, val unit: String)