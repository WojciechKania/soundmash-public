package com.adalbert.soundmash.views.renderables

import android.graphics.Canvas
import com.adalbert.soundmash.views.ITrackView

interface Renderable {
    fun render(canvas : Canvas?, trackView : ITrackView)
}