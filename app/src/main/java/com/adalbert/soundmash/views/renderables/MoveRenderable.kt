package com.adalbert.soundmash.views.renderables

import android.graphics.Canvas
import android.graphics.Paint
import com.adalbert.soundmash.views.ITrackView

class MoveRenderable(val currentMoveIndex : () -> Int) : Renderable {

    private val paintMove = Paint().apply {
        this.setARGB(255, 255, 200, 200)
    }

    override fun render(canvas: Canvas?, trackView: ITrackView) {
        val scaledMoveIndex = (currentMoveIndex() - trackView.mRenderingBeginIndex) * (trackView.mBarWidth + trackView.mBarDistance).toFloat()
        canvas?.drawLine(scaledMoveIndex, 0f, scaledMoveIndex, trackView.mHeight.toFloat(), paintMove)
        canvas?.drawLine(scaledMoveIndex + 1, 0f, scaledMoveIndex + 1, trackView.mHeight.toFloat(), paintMove) // TODO : Can generate bugs
    }

}