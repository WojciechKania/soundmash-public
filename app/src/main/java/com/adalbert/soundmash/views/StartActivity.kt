package com.adalbert.soundmash.views

import android.Manifest
import android.animation.Animator
import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.animation.addListener
import com.adalbert.soundmash.R
import kotlinx.android.synthetic.main.activity_start.*
import kotlin.random.Random
import kotlin.system.exitProcess


class StartActivity : AppCompatActivity() {

    private val mPermissionRequestCode = 4125

    override fun onCreate(savedInstanceState: Bundle?) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        supportActionBar?.hide()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        val permissionRead = applicationContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
        val permissionWrite = applicationContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (permissionRead != PackageManager.PERMISSION_GRANTED || permissionWrite!= PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO
                ),
                mPermissionRequestCode
            )
        }


    }

    fun onClick(view: View?) {
        val intent = when (view!!.id) {
            R.id.btnResumeEdition -> Intent(this, MainActivity::class.java)
            R.id.btnFile -> Intent(this, MainActivity::class.java).apply { this.putExtra("SOURCE", "FILE") }
            R.id.btnRecording -> Intent(this, RecordingActivity::class.java).apply { flags = flags or Intent.FLAG_ACTIVITY_NO_HISTORY }
            else -> null
        }
        if (intent != null) {
            this.startActivity(intent)
            btnResumeEdition.visibility = View.VISIBLE
        } else
            Toast.makeText(this, "Couldn't recognize button ID!", Toast.LENGTH_SHORT).show()
    }

    // TODO : Handle it better
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            mPermissionRequestCode -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED))
                    Toast.makeText(applicationContext, "Permissions granted!", Toast.LENGTH_SHORT).show()
                else {
                    Toast.makeText(applicationContext, "Permissions NOT granted!", Toast.LENGTH_LONG).show()
                    exitProcess(0)
                }

            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }
}