package com.adalbert.soundmash.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import androidx.core.view.GestureDetectorCompat
import com.adalbert.soundmash.gestures.CompleteGestureDetector
import com.adalbert.soundmash.gestures.CompleteGestureListener
import com.adalbert.soundmash.gestures.GestureContext
import com.adalbert.soundmash.presenters.*
import com.adalbert.soundmash.views.renderables.Renderable
import kotlin.math.*

class TrackView(context: Context, attrs: AttributeSet) : View(context, attrs), WaveBarDestination, SelectionListener, ITrackView {

    private var mWaveBarSource : WaveBarSource? = null
    private var mBarsPerSecond : Int = 0

    private val mRenderablesList = mutableListOf<Renderable>()

    override var mRenderingBeginIndex : Int = 0
    override var mBarWidth : Int = 1
    override var mBarDistance : Int = 0
    override val mWidth: Int
        get() = width
    override val mHeight: Int
        get() = height
    override val view: View
        get() = this


    private var mBeginSelection : Int? = null
    private var mEndSelection : Int? = null
    private var mGestureContext : GestureContext? = null

    private var dMaxDrawTime : Int = 0

    private val paintExtreme = Paint().apply {
        this.setARGB(255, 180, 180, 180)
    }

    private val paintRMS = Paint().apply {
        this.setARGB(255, 150, 150, 150)
    }

    private val paintBackground = Paint().apply {
        this.setARGB(255, 30, 49, 51)
    }

    private val paintBreak = Paint().apply {
        this.setARGB(255, 20, 39, 41)
    }

    private val paintTime = Paint().apply {
        this.setARGB(200, 255, 0, 0)
        this.textSize = 20f
    }

    private val paintSelection = Paint().apply {
        this.setARGB(150, 40, 59, 61)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        mGestureContext?.handleTouchEvent(event)?.let {
            if (it) return true
        }
        return super.onTouchEvent(event)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        val startTime = System.currentTimeMillis()
        canvas?.let {
            drawSamples(it)
            drawTimeScale(it)
            drawSelection(it)
            for (renderable in mRenderablesList)
                renderable.render(it, this)
        }
        val drawTime = System.currentTimeMillis() - startTime
        if (drawTime > dMaxDrawTime) {
            dMaxDrawTime = drawTime.toInt()
            Log.d("MAX_DRAW_TIME", "Max draw time = $drawTime ms")
        }
    }

    private fun drawSelection (canvas : Canvas) {
        mBeginSelection?.let { beginSelection ->
            val barSectionWidth = mBarWidth + mBarDistance
            val beginDrawingIndex = max(beginSelection * barSectionWidth - mRenderingBeginIndex * barSectionWidth, 0)
            var endDrawingIndex : Int?
            mEndSelection?.let { endSelection ->
                endDrawingIndex = min(endSelection * barSectionWidth - mRenderingBeginIndex * barSectionWidth, width)
                canvas.drawLine(beginDrawingIndex.toFloat(), 0f, beginDrawingIndex.toFloat(), height.toFloat(), paintExtreme)
                canvas.drawRect(beginDrawingIndex.toFloat(), 0f, endDrawingIndex!!.toFloat(), height.toFloat(), paintSelection)
                canvas.drawLine(endDrawingIndex!!.toFloat(), 0f, endDrawingIndex!!.toFloat(), height.toFloat(), paintExtreme)
            }
            if (mEndSelection == null)
                canvas.drawLine(beginDrawingIndex.toFloat(), 0f, beginDrawingIndex.toFloat(), height.toFloat(), paintExtreme)
        }

    }

    private fun drawTimeScale(canvas : Canvas) {
        if (mBarsPerSecond == 0)
            return

        val barSectionWidth = mBarWidth + mBarDistance
        val accuratePixelsPerSecond = mBarsPerSecond * barSectionWidth
        val accurateStartingIndex = mRenderingBeginIndex * barSectionWidth
        var secondIndex = round(10.0 * accurateStartingIndex / accuratePixelsPerSecond) / 10
        while (secondIndex * accuratePixelsPerSecond - accurateStartingIndex < width) {
            val lineX = (secondIndex * accuratePixelsPerSecond - accurateStartingIndex).toFloat()
            val isFullSecond = abs(secondIndex - floor(secondIndex)) < 0.01 || abs(secondIndex - ceil(secondIndex)) < 0.01
            canvas.drawLine(lineX, 0f, lineX,
                if (isFullSecond) 20f else 10f, paintTime)
            if (isFullSecond)
                canvas.drawText("${round(secondIndex).toInt()}s", lineX + 5, 20f, paintTime)
            canvas.drawLine(lineX, height - if (isFullSecond) 20f else 10f, lineX,
                height.toFloat(), paintTime)
            secondIndex += 0.1
        }
        canvas.drawLine(0f, 0f, width.toFloat(), 0f, paintTime)
        canvas.drawLine(0f, height.toFloat() - 1, width.toFloat(), height.toFloat() - 1, paintTime)
    }

    private fun drawSamples(canvas : Canvas) {
        canvas.drawPaint(paintBreak)

        mWaveBarSource?.let { waveBarSource ->
            if (waveBarSource.isEmpty())
                return

            // TODO : Move that somewhere reasonable
            val breakBetweenTracks = 80
            val breakBetweenChannels = 40

            val channelHeight = (height - 2 * breakBetweenTracks - (waveBarSource.numChannels - 1) * breakBetweenChannels) / waveBarSource.numChannels
            for (channelIndex in 0 until waveBarSource.numChannels) {
                var waveBarIndex = 0
                val channelHeightOffset = channelIndex * channelHeight + breakBetweenTracks + channelIndex * breakBetweenChannels
                canvas.drawRect(0f, channelHeightOffset.toFloat(), width.toFloat(), (channelHeightOffset + channelHeight).toFloat(), paintBackground)
                while(waveBarIndex + mRenderingBeginIndex < waveBarSource.numWaveBars
                    && (waveBarIndex + 1) * (mBarWidth + mBarDistance) < width) {
                    val maxPositiveYDifference = waveBarSource.getWaveBar(channelIndex, waveBarIndex + mRenderingBeginIndex).maxPositive * (channelHeight / 2)
                    val minNegativeYDifference = waveBarSource.getWaveBar(channelIndex, waveBarIndex + mRenderingBeginIndex).minNegative * (channelHeight / 2)
                    val channelMiddleY = 0.5f * channelHeight + channelHeightOffset
                    canvas.drawRect(waveBarIndex.toFloat() * (mBarWidth + mBarDistance),
                        channelMiddleY - maxPositiveYDifference,
                        (waveBarIndex.toFloat() + 1) * (mBarWidth + mBarDistance).toFloat() - mBarDistance,
                        channelMiddleY - minNegativeYDifference,
                        paintRMS)
                    val rmsPositiveYDifference = waveBarSource.getWaveBar(channelIndex, waveBarIndex + mRenderingBeginIndex).rmsPositive * (channelHeight / 2)
                    val rmsNegativeYDifference = waveBarSource.getWaveBar(channelIndex, waveBarIndex + mRenderingBeginIndex).rmsNegative * (channelHeight / 2)
                    canvas.drawRect(waveBarIndex.toFloat() * (mBarWidth + mBarDistance),
                        channelMiddleY - rmsPositiveYDifference,
                        (waveBarIndex.toFloat() + 1) * (mBarWidth + mBarDistance).toFloat() - mBarDistance,
                        channelMiddleY - rmsNegativeYDifference,
                        paintExtreme)
                    waveBarIndex++
                }
            }
        }
    }

    /*********************************************************
     **********  <<  LISTENERS UTILITY METHODS  >>  **********
     *********************************************************/

    override fun waveBarsUpdated(waveBarSource: WaveBarSource, barsPerSecond : Int) {
        this.mWaveBarSource = waveBarSource
        this.mBarsPerSecond = barsPerSecond
        invalidate()
    }

    override fun selectionUpdated(beginSelection: Int?, endSelection: Int?) {
        this.mBeginSelection = beginSelection
        this.mEndSelection = endSelection
        invalidate()
    }

    override fun registerGestureDetector(completeGestureListener : CompleteGestureListener) : CompleteGestureDetector {
        val gestureDetector = GestureDetectorCompat(context, completeGestureListener).apply {
            this.setOnDoubleTapListener(completeGestureListener)
        }
        val scaleDetector = ScaleGestureDetector(context, completeGestureListener)
        return CompleteGestureDetector(gestureDetector, scaleDetector)
    }

    override fun registerGestureContext(gestureContext: GestureContext) {
        mGestureContext = gestureContext
    }

    override fun registerRenderable(renderable: Renderable) {
        mRenderablesList.add(renderable)
    }

    override fun unregisterRenderable(renderable: Renderable) {
        mRenderablesList.remove(renderable)
    }

    override fun repaint() {
        invalidate()
    }

}