package com.adalbert.soundmash.views

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.adalbert.soundmash.R
import com.adalbert.soundmash.services.AudioRecorder
import com.adalbert.soundmash.connections.AudioInfo
import com.adalbert.soundmash.connections.connectionsImplementations.DialogAudioMiddleman
import com.adalbert.soundmash.presenters.WaveBar
import kotlinx.android.synthetic.main.activity_recording.*

class RecordingActivity : AppCompatActivity() {

    private lateinit var audioRecorder : AudioRecorder
    private val mRecordRequestCode = 1998 + 23
    private val maxRecordingTime = 5

    private val onBufferRecorded = { wavebars : Array<WaveBar> ->
        runOnUiThread {
            if (wavebars.isNotEmpty())
                pbLeftChannel.progress = (wavebars[0].maxPositive * 100).toInt()
            if (wavebars.size > 1)
                pbRightChannel.progress = (wavebars[1].maxPositive * 100).toInt()
        }
    }

    private val onRecordingSpaceFinished = {
        chronometer_recording_time.stop()
        val alertBuilder = AlertDialog.Builder(this)
        alertBuilder.setMessage("You reached maximum recording time")
            .setPositiveButton("Ok") {dialog : DialogInterface, _ ->
                dialog.dismiss()
                saveRecording()
            }
        runOnUiThread {
            alertBuilder.create().show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        supportActionBar?.hide()
        setContentView(R.layout.activity_recording)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, Array (1) { Manifest.permission.RECORD_AUDIO }, mRecordRequestCode);
        }

        max_time.text = String.format("/%02d:00", maxRecordingTime)
        pbLeftChannel.max = 100
        pbRightChannel.max = 100
        audioRecorder = AudioRecorder(2 * maxRecordingTime * 60 * 44100, DialogAudioMiddleman(this, {}, {}), onBufferRecorded, onRecordingSpaceFinished)
    }



    @SuppressLint("RestrictedApi")
    fun onClick(view : View) {
        when(view.id) {
            R.id.fbStartRecording -> {
                fbStartRecording.visibility = View.GONE
                fbCancelRecording.visibility = View.VISIBLE
                fbApplyRecording.visibility = View.VISIBLE
                audioRecorder.prepareRecorder(AudioInfo(44100, 16, 2))
                audioRecorder.startRecording()
                chronometer_recording_time.base = SystemClock.elapsedRealtime()
                chronometer_recording_time.start()
            }
            R.id.fbCancelRecording -> {
                chronometer_recording_time.stop()
                audioRecorder.stopRecording()
                onBackPressed()
            }
            R.id.fbApplyRecording -> {
                saveRecording()
            }
            else -> {}
        }
    }

    private fun saveRecording() {
        chronometer_recording_time.stop()
        audioRecorder.recordingFinishedAudioMiddleman = DialogAudioMiddleman(this, {
            audioRecorder.saveData(this) {
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra("SOURCE", "RECORDING")
                intent.putExtra("FILE_PATH", it)
                startActivity(intent)
            }
        }, {})
        audioRecorder.stopRecording()
    }
}