package com.adalbert.soundmash.views

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.adalbert.soundmash.R
import com.adalbert.soundmash.SoundMash
import com.adalbert.soundmash.presenters.ProjectPresenter


class MainActivity : AppCompatActivity() {

    private var mProjectPresenter: ProjectPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        supportActionBar?.hide()
        setContentView(R.layout.activity_main)

        if (mProjectPresenter == null)
            mProjectPresenter = (this.application as SoundMash).getProjectPresenter(this)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.let { checkIntentForSource(it) }
    }

    override fun onResume() {
        super.onResume()
        checkIntentForSource(intent)
    }

    private fun checkIntentForSource(newIntent : Intent) {
        val source = newIntent.extras?.getString("SOURCE")
        source?.let {
            when (source) {
                "FILE" ->  Thread {
                    mProjectPresenter!!.clearProject()
                    Thread.sleep(500)
                    runOnUiThread { mProjectPresenter!!.importSoundFromChosenFile() }
                }.start()
                "RECORDING" -> Thread {
                    mProjectPresenter!!.clearProject()
                    Thread.sleep(500)
                    val filePath = newIntent.extras?.getString("FILE_PATH")
                    runOnUiThread { mProjectPresenter!!.importSoundFromFile(filePath!!) }
                    newIntent.removeExtra("FILE_PATH")
                }.start()
            }
            newIntent.removeExtra("SOURCE")
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState)
    }

    override fun onBackPressed() {
        moveTaskToBack(true)
    }

    @ExperimentalStdlibApi
    fun onClick(view: View) {
      mProjectPresenter!!.handleClick(view.id)
    }


}