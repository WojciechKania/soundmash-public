package com.adalbert.soundmash.generators

import com.adalbert.soundmash.connections.Attribute
import com.adalbert.soundmash.utils.DoubleRange

class SilenceGenerator : AudioGenerator() {

    override val attributesDescription: List<Attribute>
        get() = listOf(Attribute("secondsToGenerate", "Seconds to generate", DoubleRange(0, 10), 3.0, "seconds"))
    override val generatorName: String = "Silence"

    override fun actuallyGenerate(audioBuffer: Array<FloatArray>, framesToGenerate: Int) {
        for (channel in audioBuffer)
            for (sampleIndex in 0 until framesToGenerate)
                channel[sampleIndex] = 0f
    }


}