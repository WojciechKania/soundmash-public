package com.adalbert.soundmash.generators

import com.adalbert.soundmash.connections.Attribute
import com.adalbert.soundmash.connections.AudioDestination


abstract class AudioGenerator {

    private var mAttributesMap : MutableMap<String, Double>? = null
    val attributesMap : MutableMap<String, Double>
        get() {
            mAttributesMap = mAttributesMap ?: {
                val attributesMap = mutableMapOf<String, Double>()
                for (attribute in attributesDescription)
                    attributesMap[attribute.attributeID] = attribute.defaultValue
                attributesMap
            }()
            return mAttributesMap!!
        }

    abstract val attributesDescription : List<Attribute>
    abstract val generatorName : String

    fun generateAudio(audioDestination : AudioDestination, offset : Int) {
        // TODO : Make amount of frames a method attribute
        val framesToGenerate = (mAttributesMap!!["secondsToGenerate"]!! * audioDestination.desiredAudioInfo.samplingRate).toInt()
        audioDestination.notifyDataSize(framesToGenerate, audioDestination.desiredAudioInfo.numChannels)
        val bufferSize = 4096
        val audioBuffer = Array(audioDestination.desiredAudioInfo.numChannels) { FloatArray(bufferSize)}
        var generatedFrames = 0
        while (generatedFrames < framesToGenerate) {
            val currentFramesToGenerate = when (generatedFrames + bufferSize < framesToGenerate) {
                true -> bufferSize
                false -> framesToGenerate - generatedFrames
            }
            actuallyGenerate(audioBuffer, currentFramesToGenerate)
            audioDestination.saveBuffer(audioBuffer, audioDestination.desiredAudioInfo, offset, currentFramesToGenerate)
            generatedFrames += currentFramesToGenerate
        }
        audioDestination.notifyFinishOperation()
    }

    protected abstract fun actuallyGenerate(audioBuffer : Array<FloatArray>, framesToGenerate: Int)

}