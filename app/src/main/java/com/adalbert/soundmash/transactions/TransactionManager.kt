package com.adalbert.soundmash.transactions

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.util.Log
import com.adalbert.soundmash.R
import com.adalbert.soundmash.connections.AudioMiddleman
import com.adalbert.soundmash.services.GlobalCacheManager

/**
 * Transaction manager is the main mechanism behind operations management in the application.
 */
class TransactionManager(private val activity: Activity) {

    private val mTransactionsList = mutableListOf<AudioTransaction>()
    val transactionsAmount : Int
        get() = mTransactionsList.size

    /**
     * Method used for applying given audio transactions.
     * @param audioTransaction Transaction, that needs to be applied
     * @param onTransactionApplied Function, invoked if transaction application was successful
     */
    fun applyTransaction(audioTransaction : AudioTransaction, onTransactionApplied : () -> Unit) {
        val builder = AlertDialog.Builder(activity)
        var progressDialog : Dialog? = null
        builder.setMessage("Applying changes...")
        activity.runOnUiThread() {
            progressDialog = builder.create()
            progressDialog?.setCanceledOnTouchOutside(false)
            progressDialog?.show()
        }
        audioTransaction.audioMiddleman = object : AudioMiddleman {
            override fun operationFinished(success: Boolean, message: String?, throwable: Throwable?) {
                    progressDialog?.dismiss()
                    when (success) {
                        true -> {
                            mTransactionsList.add(audioTransaction)
                            onTransactionApplied()
                        }
                        false -> {
                            Log.e("AUDIO_TRANSACTION", message, throwable)
                            builder.setTitle("Audio transaction applying error")
                                .setMessage(message)
                                .setIcon(R.drawable.error)
                            activity.runOnUiThread() { builder.create().show() }
                        }
                    }
            }
        }
        checkIfCacheOverloaded(audioTransaction.usedCacheSize)    // Ckecks, if the app doesn't use too much cache
        audioTransaction.apply()
    }

    /**
     * Method used for reverting the last applied audio transactions.
     * @param onTransactionReverted Function, invoked if transaction revertion was successful
     */
    fun revertTransaction(onTransactionReverted : () -> Unit) {
        val builder = AlertDialog.Builder(activity)
        var progressDialog : Dialog? = null
        builder.setMessage("Applying changes...")
        activity.runOnUiThread() {
            progressDialog = builder.create()
            progressDialog?.setCanceledOnTouchOutside(false)
            progressDialog?.show()
        }
        if (mTransactionsList.size > 0) {
            val transaction = mTransactionsList.last()
            transaction.audioMiddleman = object : AudioMiddleman {
                override fun operationFinished(success: Boolean, message: String?, throwable: Throwable?) {
                    progressDialog?.dismiss()
                    mTransactionsList.removeAt(mTransactionsList.size - 1)
                    onTransactionReverted()
                }
            }
            transaction.revert()
        }
    }

    /**
     * Deletes all registered transactions.
     */
    fun clearTransactions(onTransactionsCleared: () -> Unit) {
        GlobalCacheManager.clearCache()
        mTransactionsList.clear()
        onTransactionsCleared()
    }

    /**
     * Checks if cache, if additional data would be saved in it.
     * @param additionalSpace Amount of checked additional memory space
     */
    private fun checkIfCacheOverloaded(additionalSpace : Long?) {
        Log.d("CACHE_STATUS", "Cache ${if (GlobalCacheManager.isCacheOverloaded(additionalSpace ?: 0)) "is" else "isn't"} overloaded, with the size of ${(GlobalCacheManager.currentCacheSize() + (additionalSpace ?: 0)) / 1024.0} kB")
        while (mTransactionsList.isNotEmpty() && GlobalCacheManager.isCacheOverloaded(additionalSpace ?: 0))
            deleteMostDistantTransaction()
    }

    /**
     * Deletes the most distant transaction, used when the cache memory is overloaded.
     */
    private fun deleteMostDistantTransaction() {
        if (mTransactionsList.isNotEmpty()) {
            Log.d("TRANSACTION_DELETE", "Most distant transaction is being deleted")
            mTransactionsList.first().usedCacheSize?.let { GlobalCacheManager.unregisterLastFile() }
            mTransactionsList.removeAt(0)
        } else {
            Log.e("TRANSACTION_DELETE", "Non-existant transaction was trying to be deleted!")
        }
    }

}