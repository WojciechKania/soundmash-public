package com.adalbert.soundmash.transactions.transactionsImplementations

import android.util.Log
import cafe.adriel.androidaudioconverter.model.AudioFormat
import com.adalbert.soundmash.audioio.AudioIO
import com.adalbert.soundmash.models.ITrack
import com.adalbert.soundmash.connections.AudioMiddleman
import com.adalbert.soundmash.transactions.AudioTransaction
import com.adalbert.soundmash.connections.connectionsImplementations.TrackCopyAudioSource
import com.adalbert.soundmash.connections.connectionsImplementations.LogAudioMiddleman
import com.adalbert.soundmash.connections.connectionsImplementations.TrackInsertAudioDestination
import kotlin.math.min

class DeleteAudioTransaction(private val mTrack : ITrack, private val cachePath : String, private val offset : Int, private val framesToRemove : Int) : AudioTransaction {

    private var mAudioMiddleman : AudioMiddleman? = null
    override var audioMiddleman: AudioMiddleman?
        get() = mAudioMiddleman
        set(value) {
            value?.let { mAudioMiddleman = value }
        }

    override val usedCacheSize: Long? = framesToRemove.toLong() * (mTrack.validBits / 8) * mTrack.numChannels

    override fun apply() {
        AudioIO.AudioWriterInstance.writeFrames(cachePath, AudioFormat.WAV, TrackCopyAudioSource(mTrack), object : AudioMiddleman {
            // TODO : Handle failure better!
            override fun operationFinished(success: Boolean, message: String?, throwable: Throwable?) {
                // if cache file rightfully saved, remove the samples
                when (success) {
                    true -> {
                        mTrack.removeSamples(min(offset, mTrack.numFrames), min(offset + framesToRemove, mTrack.numFrames))
                        audioMiddleman?.operationFinished(true)
                    }
                    false -> {
                        Log.e("AUDIO_DELETION_CACHE_SAVE", message, throwable)
                        audioMiddleman?.operationFinished(false, message, throwable)
                    }
                }
            }
        }, offset, framesToRemove)


    }

    override fun revert() {
        AudioIO.AudioReaderInstance.readFrames("$cachePath.wav", 0, offset, null, TrackInsertAudioDestination(mTrack, audioMiddleman), LogAudioMiddleman())
    }
}