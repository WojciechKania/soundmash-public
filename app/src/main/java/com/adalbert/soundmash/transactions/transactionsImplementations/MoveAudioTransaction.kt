package com.adalbert.soundmash.transactions.transactionsImplementations

import com.adalbert.soundmash.models.ITrack
import com.adalbert.soundmash.connections.AudioMiddleman
import com.adalbert.soundmash.transactions.AudioTransaction

class MoveAudioTransaction(private val mTrack : ITrack, private val offset : Int, private val framesToMove : Int, private val moveIndex : Int) : AudioTransaction {

    private var mAudioMiddleman : AudioMiddleman? = null
    override var audioMiddleman: AudioMiddleman?
        get() = mAudioMiddleman
        set(value) {
            value?.let { mAudioMiddleman = value }
        }

    override val usedCacheSize : Long? = null

    override fun apply() {
        mTrack.moveSamples(offset, offset + framesToMove, moveIndex)
        audioMiddleman?.operationFinished(true)
    }

    override fun revert() {
        mTrack.moveSamples(moveIndex, moveIndex + framesToMove, offset)
        audioMiddleman?.operationFinished(true)
    }

}