package com.adalbert.soundmash.transactions.transactionsImplementations

import com.adalbert.soundmash.audioio.AudioIO
import com.adalbert.soundmash.models.ITrack
import com.adalbert.soundmash.connections.AudioDestination
import com.adalbert.soundmash.connections.AudioInfo
import com.adalbert.soundmash.connections.AudioMiddleman
import com.adalbert.soundmash.transactions.AudioTransaction

class LoadFileTransaction(private val filePath : String, private val mTrack : ITrack,
                          private val offsetSource : Int = 0, private val offsetDestination : Int = mTrack.numFrames, private val framesToLoad : Int?) : AudioTransaction {

    private var mAudioMiddleman : AudioMiddleman? = null
    override var audioMiddleman: AudioMiddleman?
        get() = mAudioMiddleman
        set(value) {
            value?.let { mAudioMiddleman = value }
        }

    override val usedCacheSize : Long? = null

    private var loadedFrames = 0

    override fun apply() {

        AudioIO.AudioReaderInstance.readFrames(filePath, audioDestination = object : AudioDestination {

            override val desiredAudioInfo: AudioInfo
                get() = AudioInfo(mTrack.samplingRate, mTrack.validBits, mTrack.numChannels)

            override fun openDestination() {}
            override fun closeDestination() {}

            override fun saveBuffer(audioBuffer: Array<FloatArray>, actualAudioInfo: AudioInfo, offset : Int, framesToLoad : Int) {
                mTrack.bulkInsertSamples(audioBuffer, actualAudioInfo.samplingRate, actualAudioInfo.validBits, offset, framesToLoad)
                loadedFrames += framesToLoad
            }

            override fun notifyDataSize(additionalSize: Int, numChannels: Int) {
                mTrack.assureSufficientCapacity(additionalSize, numChannels)
            }

            override fun notifyFinishOperation() {
                mAudioMiddleman?.operationFinished(true)
            }
        }, audioMiddleman = audioMiddleman, offsetSource = offsetSource, offsetDestination = offsetDestination, framesToRead = framesToLoad)
    }

    override fun revert() {
        mTrack.removeSamples(offsetDestination, offsetDestination + loadedFrames)
        audioMiddleman?.operationFinished(true, null, null)
    }
}